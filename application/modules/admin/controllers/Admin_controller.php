<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_controller extends MX_Controller {

     public function __construct()
        {
                parent::__construct();
                $this->load->model('admin_model');
        }

    public function admin_login($user){
      if($user == $this->session->userdata('admin_details')[0]['username']){
        $this->load->view('admin_dashboard');
      }
      else{
        $this->session->unset_userdata('admin_details');
        $this->session->sess_destroy();
        redirect('page_not_found');
      }

    }

    public function get_student_admission_details_as_per_search($type){
      $admissionid = $_POST['admissionid'];
      $firstname = $_POST['firstname'];
      $lastname = $_POST['lastname'];

      $user = $this->session->userdata('admin_details')[0]['username'];
      $data['csrf_token'] = $this->security->get_csrf_hash();     // for refreshing csrf token

      if($user == $this->session->userdata('admin_details')[0]['username']){
          if(($admissionid == "") && ($firstname == "") && ($lastname == "")){
            $data['num'] = "1";
            echo json_encode($data);
          }elseif (($admissionid != "") || ($firstname != "") || ($lastname != "")) {
            if($type == "get_details"){
              $details = $this->admin_model->get_admission_details_of_student($admissionid,$firstname,$lastname,$type);
              $data['message'] = "";
              $data['type'] = $type;
              if(count($details) != 0){
                $data['student_details'] = $details;
              }
              else{
                $data['message'] = "No data found.";
              }
              $data['num'] = "2";
              echo json_encode($data);
          }
        }
      }
    }

    public function get_student_subject_details($admissionid){
      $data['csrf_token'] = $this->security->get_csrf_hash();     // for refreshing csrf token

      $data['previous_exam_details'] = $this->admin_model->get_student_previuos_exam_details($admissionid);
      $data['center_details'] = $this->admin_model->get_center_details();
      $data['subject_details'] = $this->admin_model->get_student_course_data($admissionid);
      $data['message'] = "";
      if(count($data['subject_details']) != 0){
        $data['num'] = "2";
      }
      else{
        $data['num'] = "1";
        $data['message'] = "No Data Found";
      }

      echo json_encode($data);
    }

    public function book_student_exam(){
      $data['csrf_token'] = $this->security->get_csrf_hash();     // for refreshing csrf token
      $_POST['examdate'] = str_replace('/', '-', $_POST['examdate']);
      $exam_date = date("Y-m-d",strtotime($_POST['examdate']));
      $exam_time = date("h:i a",strtotime($_POST['examtime']));
      $stream = $_POST['stream'];
      $admissionid = $_POST['admission_id'];
      $moduleid = $_POST['module_id'];
      $courseid = $_POST['course_id'];
      $centerid = $_POST['centerid'];

      $exam_data = array(
        "ADMISSION_ID"=>$admissionid,
        "COURSE_ID"=>$courseid,
        "MODULE_ID"=>$moduleid,
        "EXAM_DATE"=>$exam_date,
        "EXAM_TIME"=>$exam_time,
        "CENTRE_ID"=>$centerid,
        "PRACTICAL_MARKS"=>'8',
        "PRACTICAL_MARKS_OUT_OF"=>'10',
        "JOURNAL_MARKS"=>'11',
        "JOURNAL_MARKS_OUT_OF"=>'20',
        "STREAM"=>$stream,
        "IS_EXAM_GIVEN"=>'0',
        "IS_CERIFICATE_ISSUED"=>'0',
        "EXAM_TYPE"=>'THEORY'
      );
      $booked_status = $this->admin_model->book_theory_exam($exam_data);
      if($booked_status){
        $data['num'] = "1";
      }
      else{
        $data['num'] = "0";
      }
      echo json_encode($data);
    }

    public function get_required_data_as_per_login($type){
      $username = $_POST['username'];
      $password = $_POST['password'];
      $date = "";
      if(isset($_POST['date'])){
        $date = $_POST['date'];
      }
      $user = $this->session->userdata('admin_details')[0]['username'];

      $data['csrf_token'] = $this->security->get_csrf_hash();     // for refreshing csrf token

      if($user == $this->session->userdata('admin_details')[0]['username']){
        if($type == "download_list"){
          $user_exist = $this->admin_model->validate_admin_page_for_actions($username,"supervisor");
          if($user_exist['count'] != 0){
            if($user_exist['password'] == $password){
              $exam_data = $this->admin_model->download_todays_exam_list($date);
              $data['message'] = "";
              $data['type'] = $type;
              if(count($exam_data) != 0){
                for($i=0;$i<count($exam_data);$i++){
                  $check_exam_id_exist_or_not = $this->admin_model->check_exam_if_not_insert_exam_data($exam_data[$i]['ExamID'],$exam_data[$i]);
                  if($check_exam_id_exist_or_not){
                    $insert_question_data = $this->admin_model->check_question_paper_by_course_id_if_not_insert($exam_data[$i]['Course_Id']);
                    if($insert_question_data){
                      $data['exam_data'] = $exam_data;
                    }
                  }
                  else{
                    $data['exam_data'] = $exam_data;
                  }
                }
              }
              else{
                $data['message'] = "No data found.";
              }

              $data['num'] = "4";
              echo json_encode($data);
            }
            else{
              $data['num'] = "2";
              echo json_encode($data);
            }
          }
          else{
            $data['num'] = "1";
            echo json_encode($data);
          }
        }
        elseif($type == "upload_list"){
          $user_exist = $this->admin_model->validate_admin_page_for_actions($username,"supervisor");
          if($user_exist['count'] != 0){
            if($user_exist['password'] == $password){
              $exam_data = $this->admin_model->upload_todays_exam_list($date);
              $data['message'] = "";
              $data['type'] = $type;
              if(count($exam_data) != 0){
                $data['exam_data'] = $exam_data;
              }
              else{
                $data['message'] = "No data to upload.";
              }

              $data['num'] = "4";
              echo json_encode($data);
            }
            else{
              $data['num'] = "2";
              echo json_encode($data);
            }
          }
          else{
            $data['num'] = "1";
            echo json_encode($data);
          }
        }
      }
      else{
        $data['num'] = "3";
        echo json_encode($data);
      }

    }

    public function get_exam_and_paper_details($type){
      $studentname = $_POST['studentname'];
      $fromdate = $_POST['fromdate'];
      $todate = $_POST['todate'];
      $user = $this->session->userdata('admin_details')[0]['username'];

      $data['csrf_token'] = $this->security->get_csrf_hash();     // for refreshing csrf token

      if($user == $this->session->userdata('admin_details')[0]['username']){
        if(($studentname == "") && ($fromdate == "") && ($todate == "")){
          $data['num'] = "5";
          echo json_encode($data);
        }
        else if(($studentname != "") || ($fromdate != "") || ($todate != "")){
          if($type == "exam_list"){
            $exam_list = $this->admin_model->get_exam_list_between_dates($studentname,$fromdate,$todate,$type);
            $data['message'] = "";
            $data['type'] = $type;
            if(count($exam_list) != 0){
              $data['exam_list'] = $exam_list;
            }
            else{
              $data['message'] = "No data found.";
            }
            $data['num'] = "2";
            echo json_encode($data);
          }
          elseif($type == "question_paper_backup"){
            $exam_list = $this->admin_model->get_exam_list_between_dates($studentname,$fromdate,$todate,$type);
            $data['message'] = "";
            $data['type'] = $type;
            if(count($exam_list) != 0){
              $data['exam_list'] = $exam_list;
            }
            else{
              $data['message'] = "No data found.";
            }
            $data['num'] = "2";
            echo json_encode($data);
          }
          else{
            $data['num'] = "1";
            echo json_encode($data);
          }
        }
      }
    }

    public function get_exam_paper_details($exam_id){
      $data['csrf_token'] = $this->security->get_csrf_hash();     // for refreshing csrf token

      $question_paper_backup = $this->admin_model->get_exam_backup_data($exam_id);
      if(count($question_paper_backup) != 0){
        $this->load->library('excel');
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0);

        for($k=0;$k<count($question_paper_backup['details']);$k++){
          for($l=0;$l<count($question_paper_backup['details'][$k]);$l++){
            $marks = 0;
            if($question_paper_backup['details'][$k]['CorrectAns'] == $question_paper_backup['details'][$k]['OptionSelected']){
              $marks = 1;
            }
            $question_paper_backup['details'][$k]['Marks'] = $marks;
          }
        }

        $objPHPExcel->getProperties()->setCreator("Ankur Prajapati SACL")
                                      ->setLastModifiedBy("Ankur Prajapati SACL")
                                      ->setTitle("Question Paper details");

        $objPHPExcel->getActiveSheet()->SetCellValue('A1', "Exam Id: ".$question_paper_backup['student_details'][0]['ExamID']." | Student Name: ".$question_paper_backup['student_details'][0]['StudentName']." | Course Name: ".$question_paper_backup['student_details'][0]['Course_Name']."");
        $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
        $date=date_create($question_paper_backup['student_details'][0]['Date']);
        $date = date_format($date,'d/m/Y');
        $objPHPExcel->getActiveSheet()->SetCellValue('A2', "Exam Date: ".$date." | Internal Marks: ".$question_paper_backup['student_details'][0]['InternalExamMarks']."/".$question_paper_backup['student_details'][0]['InternalOutOf']." | Online Marks: ".$question_paper_backup['student_details'][0]['Marks']."/".$question_paper_backup['student_details'][0]['outof']."");
        $objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
        $objPHPExcel->getActiveSheet()->mergeCells('A2:H2');
        $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setSize(14);
        $objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setSize(12);

        $col = 'A';
        for($i=0;$i<count($question_paper_backup['fields']);$i++){
            $objPHPExcel->getActiveSheet()->SetCellValue($col.'3', $question_paper_backup['fields'][$i]);
            $objPHPExcel->getActiveSheet()->getStyle("A3:I3")->getFont()->setBold(true);
            $col++;
        }

        $phpColorRed = new PHPExcel_Style_Color();
        $phpColorRed->setRGB('FF0000');
        $phpColorGreen = new PHPExcel_Style_Color();
        $phpColorGreen->setRGB('008000');

        $j = 4;
        $alphebetCol = 'A';
        foreach ($question_paper_backup['details'] as $key => $value) {

            foreach ($question_paper_backup['details'][$key] as $index => $data) {
                $objPHPExcel->getActiveSheet()->SetCellValue($alphebetCol.$j,$question_paper_backup['details'][$key][$index]);
                if($alphebetCol == 'H'){
                  if($question_paper_backup['details'][$key][$index] == 0){
                    $objPHPExcel->getActiveSheet()->getStyle($alphebetCol.$j)->getFont()->setColor($phpColorRed);
                  }
                  if($question_paper_backup['details'][$key][$index] == 1){
                    $objPHPExcel->getActiveSheet()->getStyle($alphebetCol.$j)->getFont()->setColor($phpColorGreen);
                  }
                }

                $alphebetCol++;
                if($alphebetCol == "I"){
                    $alphebetCol = 'A';
                }
            }
            $j++;

        }

        $objPHPExcel->getActiveSheet()->getStyle('H'.(count($question_paper_backup['details']) + 5))->getFont()->setColor($phpColorGreen);
        $objPHPExcel->getActiveSheet()->getStyle('H'.(count($question_paper_backup['details']) + 6))->getFont()->setColor($phpColorRed);

        $objPHPExcel->getActiveSheet()->SetCellValue('G'.(count($question_paper_backup['details']) + 5),"Right : ");
        $objPHPExcel->getActiveSheet()->SetCellValue('H'.(count($question_paper_backup['details']) + 5),"=SUM(H3:H".(count($question_paper_backup['details']) + 3).")");
        $wrong = "=(".count($question_paper_backup['details'])." - H".(count($question_paper_backup['details']) + 5).")";
        $total = "=SUM(H".(count($question_paper_backup['details']) + 5).":H".(count($question_paper_backup['details']) + 6).")";
        $objPHPExcel->getActiveSheet()->SetCellValue('G'.(count($question_paper_backup['details']) + 6),"Wrong : ");
        $objPHPExcel->getActiveSheet()->SetCellValue('H'.(count($question_paper_backup['details']) + 6),$wrong);
        $objPHPExcel->getActiveSheet()->SetCellValue('G'.(count($question_paper_backup['details']) + 7),"Total Marks : ");
        $objPHPExcel->getActiveSheet()->SetCellValue('H'.(count($question_paper_backup['details']) + 7),$total);
        $objPHPExcel->getActiveSheet()->getStyle("A".(count($question_paper_backup['details']) + 7).":I".(count($question_paper_backup['details']) + 7)."")->getFont()->setBold(true);

        $styleArray = array(
          'borders' => array(
            'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );

        $objPHPExcel->getActiveSheet()->getStyle('A1:H'.(count($question_paper_backup['details']) + 7))->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setTitle("Question Paper details");

        //die;
        // $filename = "question_paper_backup_" . date("h.i.s.d-m-Y") . ".xls";
        header('Content-Type: application/vnd.ms-excel');
        // header("Content-Disposition: attachment; filename=" . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_start();
        $objWriter->save("php://output");
        $xlsData = ob_get_contents();
        ob_end_clean();

        $response =  array(
            'op' => 'ok',
            'csrf_token' => $this->security->get_csrf_hash(),
            'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
        );
        echo json_encode($response);
      }

    }

}
