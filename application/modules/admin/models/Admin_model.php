<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

    // public $exam_db;
    // public $exam_sql_mis;
    // public $exam_sql_online;
    public function __construct() {
        $this->exam_db = $this->load->database('exam',TRUE);
        $this->website_db = $this->load->database('website',TRUE);
        parent::__construct();
    }

    public function validate_admin_page_for_actions($username,$role){
      $query = $this->exam_db->select('password')
               ->from('users')
               ->where('username',$username)
               ->where('role',$role)
               ->get();

      $data['count'] = $query->num_rows();
      if($data['count'] != 0){
        $data['password'] = $query->row()->password;
      }

      return $data;
    }

    public function get_admission_details_of_student($admissionid,$firstname,$lastname,$type){
      $query = $this->db->select('AM.ADMISSION_ID,concat(EM.ENQUIRY_FIRSTNAME," ",EM.ENQUIRY_LASTNAME) as ENQUIRY_NAME,DATE_FORMAT(AM.ADMISSION_DATE, "%d/%m/%Y") as ADMISSION_DATE,CM.CENTRE_NAME,AM.COURSE_TAKEN,AM.MODULE_TAKEN,AM.TOTALFEES,concat(EM.ENQUIRY_ADDRESS1,", ",EM.ENQUIRY_ADDRESS2,", ",EM.ENQUIRY_CITY,",",ENQUIRY_ZIP,", ",ENQUIRY_STATE) as enquiry_address,EM.ENQUIRY_EMAIL,concat(if(EM.ENQUIRY_TELEPHONE IS NULL,EM.ENQUIRY_PARENT_NO,EM.ENQUIRY_TELEPHONE)," ", EM.ENQUIRY_MOBILE_NO) as enquiry_contact,EM.PREFERREDED_TIME1,EM.PREFERREDED_TIME2,EM.PREFERREDED_TIME3,AM.STATUS,AM.REMARKS,AM.COURSE_STATUS_REMARKS_DATE,AM.COURSE_STATUS_REMARKS,AM.YEAR,AM.IN_TAKE,AM.IS_INSTALLMENT_PLAN_DONE,concat(EMP_FNAME," ",EMP_MIDDLENAME," ",EMP_LASTNAME) as employee_name,AM.EXAM_FEES_APPLICABLE,AM.PROCESSING_FEES_APPLICABLE')
        						->from('admission_master AM')
        						->join('centre_master CM','CM.CENTRE_ID = AM.CENTRE_ID')
        						->join('enquiry_master EM','EM.ENQUIRY_ID = AM.ENQUIRY_ID')
        						->join('employee_master EPM','EPM.EMPLOYEE_ID = AM.HANDLED_BY');
                    if ($firstname != '') {
                      $query = $this->db->like('EM.ENQUIRY_FIRSTNAME',$firstname,'both');
                    }

                    if ($lastname != '') {
                      $query = $this->db->like('EM.ENQUIRY_LASTNAME',$lastname,'both');
                    }

                    if ($admissionid != '') {
                      $query = $this->db->where('AM.ADMISSION_ID',$admissionid);
                    }
                    $query = $this->db->where('AM.ISACTIVE','1')
                                      ->order_by('AM.ADMISSION_ID','DESC')
                                      ->limit(10)
                                      ->get();

                    $data = $query->result_array();
        return $data;
    }

    public function get_student_previuos_exam_details($admissionid){
      $query = $this->exam_db->select('*')
        						->from('exam')
                    ->where('admissionID',$admissionid)
                    ->get();

      return $query->result_array();
    }

    public function get_student_course_data($admissionid){
      $query = $this->db->select('ACT.ADMISSION_ID,ACT.COURSE_ID,CM.COURSE_NAME,ACT.MODULE_ID,MM.MODULE_NAME,CM.STREAM')
        						->from('admission_course_transaction ACT')
        						->join('course_master CM','ACT.COURSE_ID = CM.COURSE_ID')
        						->join('module_master MM','ACT.MODULE_ID = MM.MODULE_ID','left')
                    ->where('ACT.ADMISSION_ID',$admissionid)
                    ->get();

      return $query->result_array();
    }


    public function get_center_details(){
      $query = $this->db->select('CENTRE_ID,CENTRE_NAME')
        						->from('centre_master')
                    ->where('ISACTIVE','1')
                    ->get();

      return $query->result_array();
    }

    public function book_theory_exam($exam_data){
      $this->db->insert('exam_master_new',$exam_data);
      if($this->db->affected_rows() > 0){
        return true;
      }
      else{
        return false;
      }
    }

    public function download_todays_exam_list($date = ""){
      if($date){
        $date_taken = str_replace("/","-",$date);
        $exam_date = date("Y-m-d",strtotime($date_taken));
      }
      else{
        $exam_date = date('Y-m-d');
      }

      $query = $this->db->select('EM.EXAM_MASTER_NEW_ID as ExamID,EM.ADMISSION_ID as AdmissionID,CONCAT(EnM.ENQUIRY_FIRSTNAME," ",EnM.ENQUIRY_MIDDLENAME," ",EnM.ENQUIRY_LASTNAME) as StudentName,EM.COURSE_ID as Course_Id,CM.COURSE_NAME as Course_Name,EM.EXAM_DATE as Date,EM.EXAM_TIME as Time,EM.CENTRE_ID as Centre_ID,CeM.CENTRE_NAME as Centre_Name,EM.JOURNAL_MARKS as InternalExamMarks,EM.JOURNAL_MARKS_OUT_OF as InternalOutOf')
               ->from('exam_master_new EM')
               ->join('admission_master AM','EM.ADMISSION_ID=AM.ADMISSION_ID','left')
               ->join('course_master CM','EM.COURSE_ID=CM.COURSE_ID','left')
               ->join('enquiry_master EnM','AM.ENQUIRY_ID=EnM.ENQUIRY_ID','left')
               ->join('centre_master CeM','EM.CENTRE_ID=CeM.CENTRE_ID','left')
               ->where('EM.EXAM_DATE',$exam_date)
               ->where('EM.EXAM_TYPE',"THEORY")
               ->get();

      $data = $query->result_array();
      if(count($data)){
        for($i=0;$i<count($data);$i++){
          $query1 = $this->exam_db->select('ExamGiven')
                   ->from('exam')
                   ->where('ExamID',$data[$i]['ExamID'])
                   ->get();

          $data[$i]['ExamGiven'] = 0;
          $ExamGiven = $query1->row()->ExamGiven;
          if($ExamGiven == 1){
            $data[$i]['ExamGiven'] = $query1->row()->ExamGiven;
          }
        }
      }


      return $data;
    }

    public function check_exam_if_not_insert_exam_data($exam_id,$exam_data){
      $query = $this->exam_db->select('*')
               ->from('exam')
               ->where('ExamID',$exam_id)
               ->get();

      $exam_data['LastModified'] = date('Y-m-d');
      unset($exam_data['ExamGiven']);
      if($query->num_rows() == "0"){
        $this->exam_db->insert('exam',$exam_data);
        if($this->exam_db->affected_rows() > 0){
          return true;
        }
        else{
          return false;
        }
      }
      else{
        return true;
      }
    }

    public function check_question_paper_by_course_id_if_not_insert($course_id){
      $query = $this->exam_db->select('*')
               ->from('testpaper')
               ->where('CourseID',$course_id)
               ->get();

      $query1 = $this->db->select('Total')
               ->from('qpattern')
               ->where('Course_ID',$course_id)
               ->get();

      $total = $query1->row()->Total;

     if($query->num_rows() == "0"){
       $query2 = $this->website_db->select('question_name as Question,ans_a as ansA,ans_b as ansB,ans_c as ansC,ans_d as ansD,correct_ans as CorrectAns,course_id as CourseID')
                ->from('question_paper')
                ->where('course_id',$course_id)
                ->limit($total)
                ->get();
      // $query2 = $this->exam_db->select('Question,ansA,ansB,ansC,ansD,CorrectAns,33 as CourseID')
      //          ->from('testpaper')
      //          ->where('CourseID','3475')
      //          ->limit($total)
      //          ->get();
       $questions = $query2->result_array();
       for($i=0;$i<count($questions);$i++){
         $this->exam_db->insert('testpaper',$questions[$i]);
       }

       if($this->exam_db->affected_rows() > 0){
         return true;
       }
       else{
         return false;
       }
     }
     else{
       return true;
     }
    }

    public function upload_todays_exam_list($date = ""){
      if($date){
        $date_taken = str_replace("/","-",$date);
        $exam_date = date("Y-m-d",strtotime($date_taken));
      }
      else{
        $exam_date = date('Y-m-d');
      }
      $query = $this->exam_db->select('ExamID,AdmissionID,StudentName,Course_Name,Date,Time,Centre_ID,Centre_Name,InternalExamMarks,InternalOutOf,Marks,outof')
               ->from('exam')
               ->where('Date',$exam_date)
               ->where('ExamGiven',"1")
               ->get();

      $data = $query->result_array();
      if(count($data)){
        for($i=0;$i<count($data);$i++){

          $internal = $data[$i]['InternalExamMarks'];
          $internal_out_of = $data[$i]['InternalOutOf'];
          $online_marks = $data[$i]['Marks'];
          $online_out_of = $data[$i]['outof'];

          $total_marks = $internal + $online_marks;
          $total_out_of_marks = $internal_out_of + $online_out_of;

          $percentage = $total_marks/$total_out_of_marks*100;
          $result = "Fail";
          if($percentage >= 40){
            $result = "Pass";
          }

          $exam_details = array(
            'ONLINE_MARKS'	=> $online_marks,
            'ONLINE_MARKS_OUT_OF'	=> $online_out_of,
            'JOURNAL_MARKS'	=> $internal,
            'JOURNAL_MARKS_OUT_OF' => $internal_out_of,
            'IS_EXAM_GIVEN' => '1',
            'UPDATE_DATE' => $exam_date,
            'RESULT' => $result,
            'PERCENTAGE' => round($percentage)
          );

          $this->db->set($exam_details)
                        ->where('EXAM_MASTER_NEW_ID',$data[$i]['ExamID'])
                        ->where('ADMISSION_ID',$data[$i]['AdmissionID'])
                        ->update('exam_master_new');

          // $this->exam_sql_mis->set($exam_details)
          //               ->where('EXAM_MASTER_NEW_ID',$data[$i]['ExamID'])
          //               ->where('ADMISSION_ID',$data[$i]['AdmissionID'])
          //               ->update('exam_master_new');
        }
      }
      return $data;
    }

    public function get_exam_list_between_dates($studentname,$fromdate,$todate,$type){
      $ad_date_format_change = str_replace("/","-",$fromdate);
      $from_date = date('Y-m-d', strtotime($ad_date_format_change));

      $ad_to_date_format_change = str_replace("/","-",$todate);
      $to_date = date('Y-m-d', strtotime($ad_to_date_format_change));

      $where = "Date between '$from_date' and '$to_date'";
      $query = $this->exam_db->select('*')
                             ->from('exam');
                             if($type == "question_paper_backup"){
                               $query = $this->exam_db->where('ExamGiven','1');
                             }
                             if(($studentname != "") && ($fromdate != "") && ($todate != "")){
                               $query = $this->exam_db->where($where)
                                             ->like('StudentName',$studentname,'both');
                             }
                             else{
                               if(($studentname != "")){
                                 $query = $this->exam_db->like('StudentName',$studentname,'both');
                               }
                               if (($fromdate != "") && ($todate != "")) {
                                 $query = $this->exam_db->where($where);
                               }
                             }
                             $query = $this->exam_db->get();

      return $query->result_array();
    }

    public function get_exam_backup_data($exam_id){
      $query = $this->exam_db->select('Question,ansA,ansB,ansC,ansD,CorrectAns,OptionSelected,"marks" as Marks')
                              ->from('exam_backup')
                              ->where('ExamID',$exam_id)
                              ->limit('40')
                              ->order_by('SRNO','DESC')
                              ->get();

      $query1 = $this->exam_db->select('*')
                              ->from('exam')
                              ->where('ExamID',$exam_id)
                              ->get();

      $data['student_details'] = $query1->result_array();
      $data['details'] = $query->result_array();
    	$data['fields'] = $query->list_fields();

      return $data;
    }
}
