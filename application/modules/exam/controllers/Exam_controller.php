<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exam_controller extends MX_Controller {

     public function __construct()
        {
                parent::__construct();
                $this->load->model('exam_model');
        }

    public function setinstruction($exam_id){
        if($exam_id == $this->session->userdata('exam_details')[0]['examid']){
          $data['qpattern_details'] = $this->exam_model->get_minutes_by_course_id($this->session->userdata('exam_details')[0]['course_id']);
          $data['start_exam_link'] = base_url('exam/'.$this->session->userdata('exam_details')[0]['examid'].'/course/'.$data['qpattern_details'][0]['Course_ID']).'/time/'.$data['qpattern_details'][0]['Duration'].'/count/'.$data['qpattern_details'][0]['Total'];
          $this->load->view('exam_instructions',$data);
        }
        else{
          $this->session->unset_userdata('exam_details');
          $this->session->unset_userdata('exam_status_data');
          $this->session->sess_destroy();
          redirect('page_not_found');
        }
    }

    public function start_exam($exam_id,$course_id,$time,$count){
        if($exam_id == $this->session->userdata('exam_details')[0]['examid']){
          $data['exam_details'] = $this->session->userdata('exam_details');
          $this->load->view('start_exam',$data);
        }
        else{
          $this->session->unset_userdata('exam_details');
          $this->session->unset_userdata('exam_status_data');
          $this->session->sess_destroy();
          redirect('page_not_found');
        }
    }

    public function get_question_paper_details(){
      $exam_id = $_POST['exam_id'];
      $course_id = $_POST['course_id'];
      $data['question_paper'] = array();
      if($exam_id == $this->session->userdata('exam_details')[0]['examid']){
        $data['question_paper'] = $this->exam_model->get_question_paper_by_course_id($course_id);
        $data['qpattern_details'] = $this->exam_model->get_minutes_by_course_id($this->session->userdata('exam_details')[0]['course_id']);
      }
      $data['csrf_token'] = $this->security->get_csrf_hash();
      echo json_encode($data);
    }

    public function question_paper_data($exam_id){
      if($exam_id == $this->session->userdata('exam_details')[0]['examid']){
        $exam_backup_data = $this->input->post('paper_backup_data');
        $time = $this->input->post('time');
        $modified_date =  date("Y-m-d H:i", strtotime(date('Y-m-d')));
        $marks_obtained = 0;
        $out_of = count($exam_backup_data)*2;

        $exam_backup = false;
        $exam_status_data = array();
        for($i=0;$i<count($exam_backup_data);$i++){
          if(!(isset($exam_backup_data[$i]['ansA']))){
            $exam_backup_data[$i]['ansA'] = "";
          }
          if(!(isset($exam_backup_data[$i]['ansB']))){
            $exam_backup_data[$i]['ansB'] = "";
          }
          if(!(isset($exam_backup_data[$i]['ansC']))){
            $exam_backup_data[$i]['ansC'] = "";
          }
          if(!(isset($exam_backup_data[$i]['ansD']))){
            $exam_backup_data[$i]['ansD'] = "";
          }
          $exam_paper_questions =  array(
              'ExamID' => $exam_id,
              'Qno' => ($i + 1),
              'Question'  => $exam_backup_data[$i]['question'],
              'AnsA'  => $exam_backup_data[$i]['ansA'],
              'AnsB'  => $exam_backup_data[$i]['ansB'],
              'AnsC'  => $exam_backup_data[$i]['ansC'],
              'AnsD'  => $exam_backup_data[$i]['ansD'],
              'CorrectAns'  => $exam_backup_data[$i]['correctAns'],
              'OptionSelected'  => $exam_backup_data[$i]['selectedAns'],
              'TimeLeft'  => $time,
              'LastModified'  => $modified_date
          );
          $exam_backup = $this->exam_model->insert_question_paper_details_exam_backup($exam_paper_questions);
          if($exam_backup_data[$i]['correctAns'] == $exam_backup_data[$i]['selectedAns']){
            $marks_obtained += 2;
          }
          $exam_data = "";
          if($exam_backup_data[$i]['selectedAns'] != "NA"){
            if($exam_backup_data[$i]['correctAns'] == $exam_backup_data[$i]['selectedAns']){
              $exam_data = array(
                  'Qno' => ($i + 1),
                  'color'  => "#299f37"
              );
            }
            else{
              $exam_data = array(
                  'Qno' => ($i + 1),
                  'color'  => "#e31e24"
              );
            }
          }
          else{
            $exam_data = array(
                'Qno' => ($i + 1),
                'color'  => "blue"
            );
          }
          array_push($exam_status_data, $exam_data);
        }

        $data = $this->session->userdata('exam_details');
        $data[0]['mark_obtained'] = $marks_obtained;
        $data[0]['out_of'] = $out_of;

        $this->session->set_userdata('exam_details', $data);
        $this->session->set_userdata('exam_status_data', $exam_status_data);

        if($exam_backup){
            echo json_encode("1");
        }
        else{
            echo json_encode("0");
        }
      }
    }

    public function exam_status($exam_id){
      if($exam_id == $this->session->userdata('exam_details')[0]['examid']){
        $admission_id = $this->session->userdata('exam_details')[0]['admission_id'];
        $marks = $this->session->userdata('exam_details')[0]['mark_obtained'];
        $outof = $this->session->userdata('exam_details')[0]['out_of'];
        $percentage = $marks/$outof*100;
        $round_percentage = round($percentage);

        $status = "Fail";
        if($round_percentage >= 40){
          $status = "Pass";
        }

        $exam_details = array(
          'InternalExamMarks'	=> $this->session->userdata('exam_details')[0]['internal_marks'],
          'InternalOutOf'	=> $this->session->userdata('exam_details')[0]['internal_out_of'],
          'Marks'	=> $this->session->userdata('exam_details')[0]['mark_obtained'],
          'outof' => $this->session->userdata('exam_details')[0]['out_of'],
          'percentage' => $round_percentage,
          'result' => $status,
          'ExamGiven' => "1",
          'Duration' => "40",
          'TimeLeft' => "40"
        );

        $this->exam_model->update_exam_details($exam_id,$admission_id,$exam_details);
        $this->load->view('exam_status');
      }
      else{
        $this->session->unset_userdata('exam_details');
        $this->session->unset_userdata('exam_status_data');
        $this->session->sess_destroy();
        redirect('page_not_found');
      }
    }

}
