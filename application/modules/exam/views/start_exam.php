<link rel="stylesheet" href="<?php echo base_url('resources/'); ?>css/bootstrap.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url('resources/login/'); ?>js/jquery-2.2.0.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>
body {
    padding-top: 90px;
}
.panel-login {
	border-color: #ccc;
	-webkit-box-shadow: 0px 2px 3px 0px rgb(237,170,30);
	-moz-box-shadow: 0px 2px 3px 0px rgb(237,170,30);
	/* box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2); */
  box-shadow: 0px 2px 3px 0px rgb(237,170,30);
}
.panel-login>.panel-heading {
	color: #00415d;
	background-color: #fff;
	border-color: #fff;
	text-align:center;
}
.panel-login>.panel-heading a{
	text-decoration: none;
	color: #666;
	font-weight: bold;
	font-size: 15px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login>.panel-heading a.active{
	color: red;
	font-size: 18px;
}
.panel-login>.panel-heading hr{
	margin-top: 10px;
	margin-bottom: 0px;
	clear: both;
	border: 0;
	height: 1px;
	background-image: -webkit-linear-gradient(left,rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0));
	background-image: -moz-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -ms-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -o-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
}
.panel-login input[type="text"],.panel-login input[type="email"],.panel-login input[type="password"] {
	height: 45px;
	border: 1px solid #ddd;
	font-size: 16px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login input:hover,
.panel-login input:focus {
	outline:none;
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
	border-color: #ccc;
}
.btn-login {
	background-color: #000;
	outline: none;
	color: #fff;
	font-size: 14px;
	/* height: auto; */
	font-weight: normal;
	/* padding: 14px 0; */
	text-transform: uppercase;
	border-color: #000;
}
.btn-login:hover,
.btn-login:focus {
  color: #fff;
  background-color: #353535;
  border-color: #353535;
}
.forgot-password {
	text-decoration: underline;
	color: #888;
}
.forgot-password:hover,
.forgot-password:focus {
	text-decoration: underline;
	color: #666;
}

.btn-register {
	background-color: #1CB94E;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #1CB94A;
}
.btn-register:hover,
.btn-register:focus {
	color: #fff;
	background-color: #1CA347;
	border-color: #1CA347;
}

.questions {
  font-weight: bold;
  color: #00415d;
}

.question-button{
border-radius: 100%;
border: 1px solid;
height: 30px;
padding: 4px 6px;
width: 30px;
text-align: center;
cursor: pointer;
background: rgb(237,170,30);
color: #fff;
font-weight: bold;
-webkit-box-shadow: 0px 2px 3px 0px rgb(237,170,30);
-moz-box-shadow: 0px 2px 3px 0px rgb(237,170,30);
/* box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2); */
box-shadow: 0px 2px 3px 0px rgb(237,170,30);
}

.submit_test{
  border-radius: 10px;
  border: 1px solid;
  height: 32px;
  padding: 0px 10px;
  width: auto;
  text-align: center;
  cursor: pointer;
  background: rgb(237,170,30);
  color: #fff;
  font-weight: bold;
  -webkit-box-shadow: 0px 2px 3px 0px rgb(237,170,30);
  -moz-box-shadow: 0px 2px 3px 0px rgb(237,170,30);
  /* box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2); */
  box-shadow: 0px 2px 3px 0px rgb(237,170,30);
}
.next_question,
.prev_question {
  border-radius: 100%;
  border: 1px solid;
  height: 32px;
  padding: 0px 10px;
  width: auto;
  text-align: center;
  cursor: pointer;
  background: rgb(237,170,30);
  color: #fff;
  font-weight: bold;
  -webkit-box-shadow: 0px 2px 3px 0px rgb(237,170,30);
  -moz-box-shadow: 0px 2px 3px 0px rgb(237,170,30);
  /* box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2); */
  box-shadow: 0px 2px 3px 0px rgb(237,170,30);
}

.submit_test:hover,
.question-button:hover,
.next_question:hover,
.prev_question:hover {background-color: #e80007}

.submit_test:active,
.question-button:active,
.next_question:active,
.prev_question:active {
  background-color: rgb(237,170,30);
  -webkit-box-shadow: 0px 2px 3px 0px rgb(237,170,30);
  -moz-box-shadow: 0px 2px 3px 0px rgb(237,170,30);
  /* box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2); */
  box-shadow: 0px 2px 3px 0px rgb(237,170,30);
  transform: translateY(4px);
}

.overlay {
    background-color:#cbcbcb;
    position: fixed;
    width: 100%;
    height: 100%;
    z-index: 1000;
    top: 0px;
    left: 0px;
    opacity: .5; /* in FireFox */
    filter: alpha(opacity=50); /* in IE */
}
</style>

<div class="container">
    	<div class="row">
			<div class="col-md-12">
				<div class="panel panel-login">
          <div class="panel-heading">
            <div class="row"><img src="<?php echo base_url('resources/images/'); ?>logo.png" style="height: 80px;width: 230px;"><br/></div>
            <div class="row">
              <div class="col-xs-4">
              </div>
              <div class="col-xs-4">
              </div>
              <div class="col-xs-4" style="font-weight:bold;">
                Time : <span id="question-paper-interval"></span>
              </div>
            </div>
            <hr>
          </div>
					<div class="panel-body">
						<div class="row">
              <input class="examToken" type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
              <input type="hidden" id="exam-session-details" course_id="<?php echo $this->session->userdata('exam_details')[0]['course_id'];?>" exam_id="<?php echo $this->session->userdata('exam_details')[0]['examid'];?>">
              <div class="col-lg-3" style="border-right:1px solid #ec676a;">
                <div class="row">
                  <div class="col-sm-12" id="question_dashboard">
                      <!-- question keys -->
                  </div>
                </div>
							</div>
              <div class="col-lg-9">
                <div class="row">
                  <div class="col-sm-12" id="question_details">
                      <!-- question details -->
                  </div>
                </div>
							</div>
              <div class="col-lg-12">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="col-sm-4" id="hidden_data"></div>
                    <div class="col-sm-4"  id="buttons_navigation">
                        <!-- navigation buttons -->
                    </div>
                    <div class="col-sm-4"><button class='submit_test' type='submit' title='Submit test' style="background-color:#000">Submit Test</button></div>
                  </div>
                </div>
							</div>
              <div class="col-lg-12">
                <div class="row">
                  <div class="col-sm-12" id="question_paper_backup">
                    <input type="hidden" name="exam_data">
                  </div>
                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
  <script>
  var url = window.location.href;
  var split = url.split("/");
  var minute = split[10];
  // var minute = 2;    // for checking small time gap

  var date = new Date();
  var countDownDate = new Date(date.getTime() + minute*60000);

  $(document).ready(function(){

    //function for timer
      // Update the count down every 1 second
      var x = setInterval(function() {

          // Get todays date and time
          var now = new Date().getTime();

          // Find the distance between now an the count down date
          var distance = countDownDate - now;

          // Time calculations for days, hours, minutes and seconds
          var days = Math.floor(distance / (1000 * 60 * 60 * 24));
          var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
          var seconds = Math.floor((distance % (1000 * 60)) / 1000);

          // Output the result in an element with id="demo"
          $("#question-paper-interval").text(minutes + "m " + seconds + "s ");

          // If the count down is over, write some text
          if (distance < 0) {
              clearInterval(x);
              disableScreen();
              alert("Time up !!!");
              examPaperBackup("timeUp");
          }
      }, 1000);
    // ending of timer functions

    // freeze screen on time up alert
    function disableScreen() {
        var div= document.createElement("div");
        div.className += "overlay";
        document.body.appendChild(div);
    }
    // freezing screen function ends

    // for disabling key press on window
    $(window).keypress(function(){
      return false;
    });
    // ends key pressing function

    $(document).on("keydown", keydown);
    $(document).on("keyup", keyup);

    // disabling right click function
    $(window).on("contextmenu",function(){
       return false;
    });
    // end of right click disable functions

    // for getting csrf token for doing form actions
    function csrfTkn(){
      var examToken = $('.examToken').val();
      return examToken;
    }
    // end of csrf token functions

    // setting a base url for ajax call
    function base_url(page = ''){
      if (window.location.origin == 'http://localhost' || window.location.origin == 'http://localhost:'+location.port) {
        return window.location.origin + '/onlineexam/' + page;
      }
      else{
        return window.location.origin + '/' + page;
      }
    }
    // base url function ends

    // calling question paper loading function parameters are examid of student and courseid
    getQuestionPaperDetais(split[4],split[6]);

    var questionPaper = [];     // blank array made for storing question backups
    // quetion paper details with dashboard button and question view function starts
    function getQuestionPaperDetais(examId,courseId){
      var examID = $("#exam-session-details").attr("exam_id");
      if(examID == examId){
        $.ajax({
          dataType: "json",
          url: base_url('get_question_paper_details'),
          type: "post",
          data: { course_id: courseId,
                  exam_id: examID,
                  onlineexam_token: csrfTkn()},
          success: function(result){
            var returnArrayLength = result.question_paper.length;
            if(returnArrayLength){
              var urlQLength = split[10] - 1;
              $.each(result.question_paper, function(index,item){
                if(index <= urlQLength){
                  questionPaper.push(item);
                }
              });

              questionPaper.sort(function(a,b){ return 0.5 - Math.random()});  // for suffling array data

              $.each(questionPaper, function(index,item){
                var color = "";
                if(index == 0){
                  color = "style='background-color: #2e2ee0;'";
                }
                $("#question_dashboard").append("<div class='col-sm-1 question-button' id='question_number_"+(index+1)+"' "+color+" >"+(index+1)+"</div>");
              });

              showQuestionsOnPage(questionPaper);   // question show on page function with questionpaper array in parameter
            }
            else{
              $("#question_details").append("<span class='questions'>Question Paper Not Found.</span>");
            }
            $(".examToken").val(result.csrf_token);
          }
        });
      }
    }

    // quetion showing on page function starts with questions array parameter
    function showQuestionsOnPage(questionPaper){
      for(i=0;i<questionPaper.length;i++){
        var question = questionPaper[i]['Question'];
        var questionName = questionPaper[i]['Question']+i;
        var ansA = questionPaper[i]['ansA'];
        var ansB = questionPaper[i]['ansB'];
        var ansC = questionPaper[i]['ansC'];
        var ansD = questionPaper[i]['ansD'];
        var correctAns = questionPaper[i]['CorrectAns'];
        var display = "style='display: none;'";
        if(i == 0){
          display = "style='display: block;'";
        }
        $("#question_details").append("<div class='question_sets' id='question_"+(i+1)+"' "+display+"><label>Q. No.: "+(i+1)+") <span id='question_name_"+(i+1)+"'>"+question+"</span></label><div class='radio'><label><input type='radio' id='radio_1' name='"+questionName+"' data-value='"+ansA+"' tabindex='1' value='A'>"+ansA+"</label></div><div class='radio'><label><input type='radio' id='radio_2' name='"+questionName+"' data-value='"+ansB+"' tabindex='1' value='B'>"+ansB+"</label></div><div class='radio'><label><input type='radio' id='radio_3' name='"+questionName+"' data-value='"+ansC+"' tabindex='1' value='C'>"+ansC+"</label></div><div class='radio'><label><input type='radio' id='radio_4' name='"+questionName+"' data-value='"+ansD+"' tabindex='1' value='D'>"+ansD+"</label></div></div>");
        $("#hidden_data").append("<input type='hidden' id='question_correct_ans_"+(i+1)+"' value='"+correctAns+"'>");
      }

      $("#buttons_navigation").append("<button class='next_question' type='submit' title='Next question'><i class='glyphicon glyphicon-forward'></i></button>");
    }

    // capturing click event of question buttons that which quetion is going to view now on page
    $(document).on("click",".question-button",function(){
      var questionId = $(this).attr("id");
      var questionNumber = questionId.match(/\d+/);
      var totalElements = $(".question_sets").length;

      if(questionNumber == 1){
        $("#buttons_navigation").empty();
        $("#buttons_navigation").append("<button class='next_question' type='submit' title='Next question'><i class='glyphicon glyphicon-forward'></i></button>");
      }
      else if(questionNumber == totalElements){
        $("#buttons_navigation").empty();
        $("#buttons_navigation").append("<button class='prev_question' type='submit' title='Previous question'><i class='glyphicon glyphicon-backward'></i></button>");
      }
      else{
        $("#buttons_navigation").empty();
        $("#buttons_navigation").append("<button class='prev_question' type='submit' title='Previous question'><i class='glyphicon glyphicon-backward'></i></button>&nbsp;&nbsp;&nbsp;&nbsp;<button class='next_question' type='submit' title='Next question'><i class='glyphicon glyphicon-forward'></i></button>");
      }

      $(".question_sets").hide();
      $("#question_"+(parseInt(questionNumber))).show();
      $("#"+questionId).css("background-color","#2e2ee0");
    });

    // capturing click event of next question button on page
    $(document).on("click",".next_question",function(){
      var questionId = $(".question_sets:visible").attr("id");
      var questionNumber = questionId.match(/\d+/);
      var totalElements = $(".question_sets").length;

      if(questionNumber == (totalElements -1)){
        $("#buttons_navigation").empty();
        $("#buttons_navigation").append("<button class='prev_question' type='submit' title='Previous question'><i class='glyphicon glyphicon-backward'></i></button>");
      }
      else{
        if(questionNumber >= 1){
          $("#buttons_navigation").empty();
          $("#buttons_navigation").append("<button class='prev_question' type='submit' title='Previous question'><i class='glyphicon glyphicon-backward'></i></button>&nbsp;&nbsp;&nbsp;&nbsp;<button class='next_question' type='submit' title='Next question'><i class='glyphicon glyphicon-forward'></i></button>");
        }
      }

      $("#"+questionId).hide();
      $("#question_"+(parseInt(questionNumber)+1)).show();
      $("#question_number_"+(parseInt(questionNumber)+1)).css("background-color","#2e2ee0");
    });

    // capturing click event of previous question button on page
    $(document).on("click",".prev_question",function(){
      var questionId = $(".question_sets:visible").attr("id");
      var questionNumber = questionId.match(/\d+/);
      var totalElements = $(".question_sets").length;

      if(questionNumber == 2){
        $("#buttons_navigation").empty();
        $("#buttons_navigation").append("<button class='next_question' type='submit' title='Next question'><i class='glyphicon glyphicon-forward'></i></button>");
      }
      else{
        if(questionNumber >= 1){
          $("#buttons_navigation").empty();
          $("#buttons_navigation").append("<button class='prev_question' type='submit' title='Previous question'><i class='glyphicon glyphicon-backward'></i></button>&nbsp;&nbsp;&nbsp;&nbsp;<button class='next_question' type='submit' title='Next question'><i class='glyphicon glyphicon-forward'></i></button>");
        }
      }

      $("#"+questionId).hide();
      $("#question_"+(parseInt(questionNumber)-1)).show();
      $("#question_number_"+(parseInt(questionNumber)-1)).css("background-color","#2e2ee0");
    });

    // final capturing of submit test button event for submiting paper
    $(".submit_test").click(function(){
      examPaperBackup("normal"); // function for taking question paper backup in database with parameters normal and timeup
    });

    // exampaper backup funtion starts
    function examPaperBackup(submissionType){
      var examID = $("#exam-session-details").attr("exam_id");
      var time = split[9];
      var examBackup = [];
      var totalElements = $(".question_sets").length;
      var i = 0;
      var unattempted = [];
      for(i=0;i<totalElements;i++){
        var data = {};
        var question = $("#question_name_"+(i+1)).text();
        var ansA = $("#question_"+(i+1)).find("#radio_1").attr("data-value");
        var ansB = $("#question_"+(i+1)).find("#radio_2").attr("data-value");
        var ansC = $("#question_"+(i+1)).find("#radio_3").attr("data-value");
        var ansD = $("#question_"+(i+1)).find("#radio_4").attr("data-value");
        var correctAns = $("#question_correct_ans_"+(i+1)).val();
        var selectedAns = $("#question_"+(i+1)+" input:radio:checked").val();
        if(selectedAns == undefined){
          selectedAns = "NA";
        }

        var qnoNA = 0;
        if(selectedAns == "NA"){
          qnoNA = (i+1);
        }
        unattempted.push(qnoNA);

        data.question = question;
        data.ansA = ansA;
        data.ansB = ansB;
        data.ansC = ansC;
        data.ansD = ansD;
        data.correctAns = correctAns;
        data.selectedAns = selectedAns;
        examBackup.push(data);
      }

      if(submissionType != "timeUp"){
        var j = 0;
        var qns = "";
        for(j=0;j<unattempted.length;j++){
          if(unattempted[j] != 0){
            qns += unattempted[j]+",";
            $("#question_number_"+unattempted[j]).css("background-color","rgb(237,170,30)");
          }
        }

        if(qns.length != 0){
          alert("You are not attempted yet question numbers "+qns.replace(/,\s*$/, ""));
        }
        else{
          if(confirm("Are you sure you want to Submit this?")){
              exam_status(examBackup,examID,time);
          }
          else{
              return false;
          }
        }
      }
      else{
        exam_status(examBackup,examID,time);
      }
    }
    // examPaperBackup function ends

    // function for showing exam status with next page redirection
    function exam_status(examBackup,examID,time){
      $.ajax({
         dataType: "json",
         url: base_url('question_paper_data/'+examID),
         type: "post",
         data: { time: time,
                 paper_backup_data: examBackup,
                 onlineexam_token: csrfTkn()},
         success: function(result){
           var cust_url = base_url('exam_status/'+examID+'/result');
           if(result == 1){
             window.location.href = cust_url;
           }
           else{
             examPaperBackup();
           }
         }
      });
    }
    // end of exam_status function

  });

      function keydown(e) {

          if ((e.which || e.keyCode) == 116 || ((e.which || e.keyCode) == 82 && ctrlKeyDown)) {
              // Pressing F5 or Ctrl+R
              e.preventDefault();
          } else if ((e.which || e.keyCode) == 17) {
              // Pressing  only Ctrl
              ctrlKeyDown = true;
          }
      };

      function keyup(e){
          // Key up Ctrl
          if ((e.which || e.keyCode) == 17)
              ctrlKeyDown = false;
      };
  </script>
