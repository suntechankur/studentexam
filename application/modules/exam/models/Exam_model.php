<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exam_model extends CI_Model {

    public $exam_db;
    public function __construct() {
        $this->exam_db = $this->load->database('exam',TRUE);
        parent::__construct();
    }

    public function get_minutes_by_course_id($course_name){
      $query = $this->db->select('Duration,Total,Course_ID')
               ->from('qpattern')
               ->where('Course_ID',$course_name)
               ->where('ISACTIVE',1)
               ->get();

      return $query->result_array();
    }

    public function get_question_paper_by_course_id($course_id){
      $query = $this->exam_db->select('QNo,Question,ansA,ansB,ansC,ansD,CorrectAns')
               ->from('testpaper')
               ->where('CourseID',$course_id)
               ->get();

      return $query->result_array();
    }

    public function insert_question_paper_details_exam_backup($questions_detail){
      $this->exam_db->insert('exam_backup',$questions_detail);
      if($this->exam_db->affected_rows() > 0){
        return true;
      }
      else{
        return false;
      }
    }

    public function update_exam_details($exam_id,$admission_id,$exam_details){
      $this->exam_db->set($exam_details)
                    ->where('ExamID',$exam_id)
                    ->where('AdmissionID',$admission_id)
                    ->update('exam');
      if($this->exam_db->affected_rows() > 0){
        return true;
      }
      else{
        return false;
      }
    }

}
