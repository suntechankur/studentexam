<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron_jobs_model extends CI_Model {

  public function __construct() {
      parent::__construct();
  }

  public function todays_bithday_list_of_employees(){

    $today = date('m-d');

    $query = $this->db->select('EM.EMPLOYEE_ID,CONCAT(EM.EMP_FNAME, " ", EM.EMP_LASTNAME) As EMPLOYEE_NAME,EM.DATEOFBIRTH,CM.CENTRE_NAME,CF.CONTROLFILE_VALUE as DESIGNATION,EM.EMP_EMAIL,EM.EMP_OFFICIAL_EMAIL')
                  ->from('employee_master EM')
                  ->join('centre_master CM','EM.CENTRE_ID=CM.CENTRE_ID','left')
                  ->join('control_file CF','EM.CURRENT_DESIGNATION=CF.CONTROLFILE_ID','left')
                  ->where('EM.ISACTIVE','1')
                  // ->where('EMPLOYEE_ID','3170')
                  ->like('EM.DATEOFBIRTH',$today,'both')
                  ->get();

    return  $query->result_array();
  }

  public function todays_applaud_of_employees(){

    $today = date('m-d');

    $query = $this->db->select('EM.EMPLOYEE_ID,EM.EMP_FNAME,CONCAT(EM.EMP_FNAME, " ", EM.EMP_LASTNAME) As EMPLOYEE_NAME,EM.DATEOFJOIN,CM.CENTRE_NAME,CF.CONTROLFILE_VALUE,EM.EMP_EMAIL,EM.EMP_OFFICIAL_EMAIL,TIMESTAMPDIFF(YEAR, DATEOFJOIN, NOW()) as YEAR')
                  ->from('employee_master EM')
                  ->join('centre_master CM','EM.CENTRE_ID=CM.CENTRE_ID','left')
                  ->join('control_file CF','EM.CURRENT_DESIGNATION=CF.CONTROLFILE_ID','left')
                  ->where('EM.ISACTIVE','1')
                  ->like('EM.DATEOFJOIN',$today,'both')
                  ->get();

    return  $query->result_array();
  }

  public function birthday_list_of_employees(){

    $where = "DATE_FORMAT(DATEOFBIRTH, '%m') = DATE_FORMAT(NOW() ,  '%m') + 1";
    $query = $this->db->select('EM.EMPLOYEE_ID,CONCAT(EM.EMP_FNAME, " ", EM.EMP_LASTNAME) As EMPLOYEE_NAME,CM.CENTRE_NAME,CF.CONTROLFILE_VALUE,EM.EMP_EMAIL,EM.EMP_OFFICIAL_EMAIL,DATE_FORMAT(EM.DATEOFBIRTH, "%D %M")  AS DATEOFBIRTH,DATE_FORMAT(EM.DATEOFBIRTH, "%d %m")  AS DATE,DATE_FORMAT(EM.DATEOFBIRTH, "%M")  AS month,DATE_FORMAT(NOW(), "%Y")  AS year')
                  ->from('employee_master EM')
                  ->join('centre_master CM','EM.CENTRE_ID=CM.CENTRE_ID','left')
                  ->join('control_file CF','EM.CURRENT_DESIGNATION=CF.CONTROLFILE_ID','left')
                  ->where('EM.ISACTIVE','1')
                  ->where($where)
                  ->order_by('DATE','asc')
                  ->get();

    return  $query->result_array();
  }

  public function _randomsleep()
  {
   $sleep = rand(11, 61);
   sleep($sleep);
   $this->db->reconnect();
  }

  public function get_mail_ids($centre_id)
  {
    $query = $this->db->select('email1')
                  ->from('centre_master')
                  ->where('centre_id',$centre_id)
                  ->get();

    return  $query->row()->email1;
  }

}
