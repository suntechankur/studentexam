<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>St. Angelo's Login Page </title>

    <link rel="stylesheet" href="<?php echo base_url('resources/'); ?>css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url('resources/'); ?>css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url('resources/'); ?>css/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url('resources/'); ?>css/jquery.css" type="text/css" />
    <script type="text/javascript" src="<?php echo base_url('resources/login/'); ?>js/jquery-2.2.0.js"></script>

<style type="text/css">
*, *:before, *:after {
  box-sizing: border-box;
}

html {
	overflow-y: scroll;
}

body {
  background: #ffffff;   /*#c1bdba; */
  font-family: 'Titillium Web', sans-serif;
}

a {
  text-decoration:none;
  color:#1ab188;
  transition:.5s ease;
  &:hover {
    color:darken(#1ab188,5%);
  }
}

.form {
  background:rgba(#13232f,.9);
  padding: 40px;
  max-width:600px;
  margin:40px auto;
  border-radius:4px;
  box-shadow:0 4px 10px 4px rgba(#13232f,.3);
}

.tab-group {
  list-style:none;
  padding:0;
  margin:0 0 40px 0;
  &:after {
    content: "";
    display: table;
    clear: both;
  }
  li a {
    display:block;
    text-decoration:none;
    padding:15px;
    background:rgba(#a0b3b0,.25);
    color:#a0b3b0;
    font-size:20px;
    float:left;
    width:50%;
    text-align:center;
    cursor:pointer;
    transition:.5s ease;
    &:hover {
      background:darken(#000000,5%); /*darken(#1ab188,5%);*/
      color:#ffffff;
    }
  }
  .active a {
    background:#000000;
    color:#ffffff;
  }
}

.tab-content > div:last-child {
  display:none;
}


h1 {
  text-align:center;
  color: #ff0202;   /*#ffffff;*/;
  font-weight:300;
  margin:0 0 40px;
}

label {
  position:absolute;
  transform:translateY(6px);
  left:13px;
  color:rgba(#ffffff,.5);
  transition:all 0.25s ease;
  -webkit-backface-visibility: hidden;
  pointer-events: none;
  font-size:22px;
  .req {
  	margin:2px;
  	color:#1ab188;
  }
}

label.active {
  transform:translateY(50px);
  left:2px;
  font-size:14px;
  .req {
    opacity:0;
  }
}

label.highlight {
	color:#ffffff;
}

input, textarea {
  font-size:22px;
  display:block;
  width:100%;
  height:100%;
  padding:5px 10px;
  background:none;
  background-image:none;
  border:1px solid #a0b3b0;
  color:#ffffff;
  border-radius:0;
  transition:border-color .25s ease, box-shadow .25s ease;
  &:focus {
		outline:0;
		border-color:#1ab188;
  }
}

textarea {
  border:2px solid #a0b3b0;
  resize: vertical;
}

.field-wrap {
  position:relative;
  margin-bottom:40px;
}

.top-row {
  &:after {
    content: "";
    display: table;
    clear: both;
  }

  > div {
    float:left;
    width:48%;
    margin-right:4%;
    &:last-child {
      margin:0;
    }
  }
}

.button {
  border:0;
  outline:none;
  border-radius:0;
  padding:15px 0;
  font-size:2rem;
  font-weight:600;
  text-transform:uppercase;
  letter-spacing:.1em;
  background: #ff0202;     /*#1ab188;*/
  color:#ffffff;
  transition:all.5s ease;
  -webkit-appearance: none;
  &:hover, &:focus {
    background:darken(#1ab188,5%);   /*#1ab188-dark;*/
  }
}

.button-block {
  display:block;
  width:100%;
}

.forgot {
  margin-top:-20px;
  text-align:right;
}

.tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}
</style>
</head>
<body>
  <div class="form">
    <div class="tab">
      <a href="#adminlogin"><button class="tablinks tab">Admin login</button></a>
      <a href="#examlogin"><button class="tablinks tab">Exam login</button></a>
      <a href="#relogin"><button class="tablinks tab">Exam Re-login</button></a>
    </div>

    <ul class="tab-group">
<li class="tab active"><a href="#signup">Sign Up</a></li>
<li class="tab"><a href="#login">Log In</a></li>
</ul>

        <div class="tab-content">
          <div id="adminlogin">
            <br/>
            <h1 style="color:#ff0202;">Admin Login</h1>
            <form action="/" method="post">
            <div class="top-row">
              <div class="field-wrap">
                <label>
                  User Name : <span class="req">*</span>
                </label>
                <input type="text" required autocomplete="off" />
              </div>

              <div class="field-wrap">
                <label>
                  Password : <span class="req">*</span>
                </label>
                <input type="text"required autocomplete="off"/>
              </div>
            </div>
            <button type="submit" class="button button-block"/>Log In</button>
            </form>
          </div>

          <div id="examlogin">
            <br/>
            <h1 style="color:#ff0202;">Exam Login</h1>
            <form action="/" method="post">
              <div class="field-wrap">
              <label>
                Exam Id : <span class="req">*</span>
              </label>
              <input type="email"required autocomplete="off"/>
            </div>
            <div class="field-wrap">
              <label>
                Supervisor code : <span class="req">*</span>
              </label>
              <input type="password"required autocomplete="off"/>
            </div>
            <button class="button button-block"/>Log In</button>
            </form>
          </div>

          <div id="relogin">
            <br/>
            <h1 style="color:#ff0202;">Exam Re - Login</h1>
            <form action="/" method="post">
              <div class="field-wrap">
              <label>
                Exam Id : <span class="req">*</span>
              </label>
              <input type="email"required autocomplete="off"/>
            </div>
            <div class="field-wrap">
              <label>
                Supervisor code : <span class="req">*</span>
              </label>
              <input type="password"required autocomplete="off"/>
            </div>
            <button class="button button-block"/>Re Log In</button>
            </form>
          </div>

        </div><!-- tab-content -->

  </div> <!-- /form -->

<script>
$('.form').find('input, textarea').on('keyup blur focus', function (e) {

  var $this = $(this),
      label = $this.prev('label');

	  if (e.type === 'keyup') {
			if ($this.val() === '') {
          label.removeClass('active highlight');
        } else {
          label.addClass('active highlight');
        }
    } else if (e.type === 'blur') {
    	if( $this.val() === '' ) {
    		label.removeClass('active highlight');
			} else {
		    label.removeClass('highlight');
			}
    } else if (e.type === 'focus') {

      if( $this.val() === '' ) {
    		label.removeClass('highlight');
			}
      else if( $this.val() !== '' ) {
		    label.addClass('highlight');
			}
    }

});

$('.tab a').on('click', function (e) {

  e.preventDefault();

  $('.tablinks').removeClass('active');
  $(this).find('.tablinks').addClass('active');

  target = $(this).attr('href');

  $('.tab-content > div').not(target).hide();

  $(target).fadeIn(600);

});
</script>

</body>
</html>
