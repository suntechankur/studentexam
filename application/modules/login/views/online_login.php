<head>
    <title>Suntech Computer Education</title>
    <link rel="shortcut icon" href="<?php echo base_url('resources/images'); ?>/favicon.png" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url('resources/images'); ?>/favicon.png" type="image/x-icon">

    <link rel="stylesheet" href="<?php echo base_url('resources/'); ?>css/bootstrap.css" type="text/css" />
    <script type="text/javascript" src="<?php echo base_url('resources/login/'); ?>js/jquery-2.2.0.js"></script>
    <!------ Include the above in your HEAD tag ---------->
    <style>
    body {
        padding-top: 90px;
    }
    .panel-login {
    	border-color: #ccc;
    	-webkit-box-shadow: 0px 2px 3px 0px rgb(237,170,30);
    	-moz-box-shadow: 0px 2px 3px 0px rgb(237,170,30);
    	/* box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2); */
      box-shadow: 0px 2px 3px 0px rgb(237,170,30);
    }
    .panel-login>.panel-heading {
    	color: #00415d;
    	background-color: #fff;
    	border-color: #fff;
    	text-align:center;
    }
    .panel-login>.panel-heading a{
    	text-decoration: none;
    	color: #666;
    	font-weight: bold;
    	font-size: 15px;
    	-webkit-transition: all 0.1s linear;
    	-moz-transition: all 0.1s linear;
    	transition: all 0.1s linear;
    }
    .panel-login>.panel-heading a.active{
    	color: rgb(237,170,30);
    }
    .panel-login>.panel-heading hr{
    	margin-top: 10px;
    	margin-bottom: 0px;
    	clear: both;
    	border: 0;
    	height: 1px;
    	background-image: -webkit-linear-gradient(left,rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0));
    	background-image: -moz-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
    	background-image: -ms-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
    	background-image: -o-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
    }
    .panel-login input[type="text"],.panel-login input[type="email"],.panel-login input[type="password"] {
    	border: 1px solid #ddd;
    	font-size: 12px;
    	-webkit-transition: all 0.1s linear;
    	-moz-transition: all 0.1s linear;
    	transition: all 0.1s linear;
    }
    .panel-login input:hover,
    .panel-login input:focus {
    	outline:none;
    	-webkit-box-shadow: none;
    	-moz-box-shadow: none;
    	box-shadow: none;
    	border-color: #ccc;
    }
    .btn-login {
    	background-color: #000;
    	outline: none;
    	color: #fff;
    	font-size: 14px;
    	/* height: auto; */
    	font-weight: normal;
    	/* padding: 14px 0; */
    	text-transform: uppercase;
    	border-color: #000;
    }
    .btn-login:hover,
    .btn-login:focus {
    	color: #fff;
    	background-color: #353535;
    	border-color: #353535;
    }
    .forgot-password {
    	text-decoration: underline;
    	color: #888;
    }
    .forgot-password:hover,
    .forgot-password:focus {
    	text-decoration: underline;
    	color: #666;
    }

    .btn-register {
    	background-color: #1CB94E;
    	outline: none;
    	color: #fff;
    	font-size: 14px;
    	/* height: auto; */
    	font-weight: normal;
    	/* padding: 14px 0; */
    	text-transform: uppercase;
    	border-color: #1CB94A;
    }
    .btn-register:hover,
    .btn-register:focus {
    	color: #fff;
    	background-color: #1CA347;
    	border-color: #1CA347;
    }
    </style>

</head>



<div class="container">
    	<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="panel panel-login">
					<div class="panel-heading">
            <div class="row"><br/><img src="<?php echo base_url('resources/images/'); ?>logo.png" style="height: 80px;width: 230px;"><br/><br/></div>
						<div class="row">
							<div class="col-xs-6">
								<a href="#" id="admin-login-form-link">Admin Login</a>
							</div>
							<div class="col-xs-6">
								<a href="#" class="active" id="exam-login-form-link">Exam Login</a>
							</div>
							<!-- <div class="col-xs-4">
								<a href="#" id="re-exam-login-form-link">Re-exam Login</a>
							</div> -->
						</div>
						<hr>
					</div>
					<div class="panel-body" style="background-color:rgb(237,170,30);padding:28px 28px 12px 28px;">
						<div class="row">
							<div class="col-lg-12">
								<form id="admin-form" action="" method="post" role="form" style="display: none;">
                  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
									<div class="form-group">
										<input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="">
									</div>
									<div class="form-group">
										<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-4 col-sm-offset-4">
												<input type="submit" name="admin-login" id="admin-login" tabindex="4" class="form-control btn btn-login" value="Log In">
											</div>
										</div>
									</div>
								</form>
								<form id="exam-form" action="" method="post" role="form" style="display: block;">
                  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                  <div class="form-group">
										<input type="text" name="exam-id" id="exam-id" tabindex="1" class="form-control" placeholder="Exam ID" value="">
									</div>
									<div class="form-group">
										<input type="password" name="supervisor-code" id="supervisor-code" tabindex="2" class="form-control" placeholder="Supervisor Code">
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-4 col-sm-offset-4">
												<input type="submit" name="exam-login" id="exam-login" tabindex="4" class="form-control btn btn-login" value="Log In">
											</div>
										</div>
									</div>
								</form>
								<form id="exam-relogin-form" action="" method="post" role="form" style="display: none;">
                  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                  <div class="form-group">
										<input type="text" name="re-exam-id" id="re-exam-id" tabindex="1" class="form-control" placeholder="Exam ID" value="">
									</div>
									<div class="form-group">
										<input type="password" name="re-exam-supervisor-code" id="re-exam-supervisor-code" tabindex="2" class="form-control" placeholder="Supervisor Code">
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-4 col-sm-offset-4">
												<input type="submit" name="re-exam-login" id="re-exam-login" tabindex="4" class="form-control btn btn-login" value="Log In">
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

  <script>
  $(function() {

      $('#admin-login-form-link').click(function(e) {
  		$("#admin-form").delay(100).fadeIn(100);
   		$("#exam-form").fadeOut(100);
   		$("#exam-relogin-form").fadeOut(100);
  		$('#exam-login-form-link').removeClass('active');
  		$('#re-exam-login-form-link').removeClass('active');
  		$(this).addClass('active');
  		e.preventDefault();
  	});
  	$('#exam-login-form-link').click(function(e) {
  		$("#exam-form").delay(100).fadeIn(100);
   		$("#admin-form").fadeOut(100);
   		$("#exam-relogin-form").fadeOut(100);
  		$('#admin-login-form-link').removeClass('active');
  		$('#re-exam-login-form-link').removeClass('active');
  		$(this).addClass('active');
  		e.preventDefault();
  	});
  	$('#re-exam-login-form-link').click(function(e) {
  		$("#exam-relogin-form").delay(100).fadeIn(100);
   		$("#admin-form").fadeOut(100);
   		$("#exam-form").fadeOut(100);
  		$('#admin-login-form-link').removeClass('active');
  		$('#exam-login-form-link').removeClass('active');
  		$(this).addClass('active');
  		e.preventDefault();
  	});

  });
  </script>
