<!doctype html>
<html lang="en">

<head>
    <title>St. Angelo's Login Page </title>
    <link rel="stylesheet" href="<?php echo base_url('resources/login/'); ?>css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url('resources/'); ?>css/bootstrap.css" type="text/css" />
    <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
    <script type="text/javascript" src="<?php echo base_url('resources/login/'); ?>js/jquery-2.2.0.js"></script>
    <script type="text/javascript" src="<?php echo base_url('resources/login/'); ?>js/jquery.mmenu.all.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('resources/login/'); ?>js/custom.js"></script>
    <style>
        .dash-button,
        .dash-button-hide {
            display: none;
        }
        .tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}
    </style>
</head>

<body>
    <div id="page">
       <?php echo form_open(); ?>
<!--        <form action="lib/check-login.php" method="post" name="frm">-->
            <section id="login">
                <div class="static">
                    <marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();"><h3>For personal attendance related query, check it on old emis with your respective login credentials.</h3></marquee>
                    <div class="login">
                        <div id="box">
                            <h2><img src="<?php echo base_url('resources/login/'); ?>img/login-image.png" /><span>Login for eMIS</span></h2>

                            </div>

                        <div id="login_div">
                          <div class="tab">
                            <button class="tablinks" onclick="openCity(event, 'London')">London</button>
                            <button class="tablinks" onclick="openCity(event, 'Paris')">Paris</button>
                            <button class="tablinks" onclick="openCity(event, 'Tokyo')">Tokyo</button>
                          </div>

                          <div id="London" class="tabcontent">
                            <h3>London</h3>
                            <p>London is the capital city of England.</p>
                          </div>

                          <div id="Paris" class="tabcontent">
                            <h3>Paris</h3>
                            <p>Paris is the capital of France.</p>
                          </div>

                          <div id="Tokyo" class="tabcontent">
                            <h3>Tokyo</h3>
                            <p>Tokyo is the capital of Japan.</p>
                          </div>
                          </div>
                    </div>
                </div>
            </section>
<!--        </form>-->
        <?php echo form_close(); ?>
        <script src="js/validator.min.js"></script>
        <script src="js/widgets.js"></script>
    </div>
</body>

</html>
