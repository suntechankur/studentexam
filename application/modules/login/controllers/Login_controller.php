<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_controller extends MX_Controller {

     public function __construct()
        {
                parent::__construct();
                $this->clear_cache();
                $this->load->model('login_model');
        }


    function clear_cache()
    {
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
    }

    public function admin_credentials(){
        $user_name_or_exam_id = "";
        $password_or_supervisor_code = "";
        if(isset($_POST['admin-login'])){
          $user_name_or_exam_id = $this->input->post('username');
          $password_or_supervisor_code = $this->input->post('password');
          $this->validate_exam_credentials($user_name_or_exam_id,$password_or_supervisor_code,"admin");
        }
        elseif (isset($_POST['exam-login'])) {
            $user_name_or_exam_id = $this->input->post('exam-id');
            $password_or_supervisor_code = $this->input->post('supervisor-code');
            $this->validate_exam_credentials($user_name_or_exam_id,$password_or_supervisor_code,"exam");
        }
        elseif (isset($_POST['re-exam-login'])) {
            $user_name_or_exam_id = $this->input->post('re-exam-id');
            $password_or_supervisor_code = $this->input->post('re-exam-supervisor-code');
            $this->validate_exam_credentials($user_name_or_exam_id,$password_or_supervisor_code,"re-exam");
        }
        $this->load->view('online_login');
    }

    public function validate_exam_credentials($user_name_or_exam_id,$password_or_supervisor_code,$type){
      if($type == "exam"){
        $user_exist = $this->login_model->check_user_exist($user_name_or_exam_id,$type);
        if($user_exist['count']){
          $supervisor_code = "00".$user_exist['id'];
          
          if($password_or_supervisor_code == $supervisor_code){
            $data['exam_details'] = $this->login_model->give_access_to_user_exam($user_name_or_exam_id,$user_exist['id'],$type);
            if($data['exam_details'][0]['exam_given'] == "1"){
              echo "<script>alert('Exam Already Given.');</script>";
            }
            else{
              $this->session->set_userdata($data);

              $get_exam_status = $this->login_model->get_more_exam_status($data['exam_details']);

              $data = $this->session->userdata('exam_details');
              $data[0]['project_marks'] = $get_exam_status[0]['project_marks'];
              $data[0]['project_marks_out_of'] = $get_exam_status[0]['project_marks_out_of'];

              $this->session->set_userdata('exam_details', $data);

              redirect(base_url('instructions/'.$user_name_or_exam_id));
            }
          }
          else{
            echo "<script>alert('Supervisor Code is incorrect.');</script>";
          }
        }
        else{
          echo "<script>alert('Check username is proper or not.');</script>";
        }
      }
      elseif($type == "admin") {
        $user_exist = $this->login_model->check_user_exist($user_name_or_exam_id,$type);
        if($user_exist['count']){
          $data['admin_details'] = $this->login_model->give_access_to_user_exam($user_exist['user'],$password_or_supervisor_code,$type);
          if(!empty($data['admin_details'])){
            if($data['admin_details'][0]['username'] == $user_exist['user']){
              // echo "<script>alert('Welcome ".$user_exist['user']."');</script>";
              $this->session->set_userdata($data);
              redirect(base_url('admin_login/'.$user_exist['user']));
            }
            else{
              echo "<script>alert('Password is wrong.');</script>";
            }
          }
          else{
            echo "<script>alert('Check username and password is proper or not.');</script>";
          }
        }
        else{
          echo "<script>alert('Check username and password is proper or not.');</script>";
        }
      }
      elseif($type == "re-exam"){
        echo "<script>alert('Check username and password is proper or not.');</script>";
      }

    }

    public function logout(){
        $this->session->unset_userdata('exam_details');
        $this->session->unset_userdata('exam_status_data');
        $this->session->sess_destroy();
        redirect(base_url());
    }

    public function admin_logout(){
        $this->session->unset_userdata('admin_details');
        $this->session->sess_destroy();
        redirect(base_url());
    }
}
