<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

    public $exam_db;
    public function __construct() {
        $this->exam_db = $this->load->database('exam',TRUE);
        parent::__construct();
    }

    public function check_user_exist($user_name_or_exam_id,$type){
      if($type != "admin"){
        $query = $this->exam_db->select('Centre_ID')
                 ->from('exam')
                 ->where('ExamID',$user_name_or_exam_id)
                 ->get();

          $data['count'] = $query->num_rows();
          if($data['count'] == 1){
            $data['id'] = $query->row()->Centre_ID;
          }
        return $data;
      }
      else{
        $query = $this->exam_db->select('username')
                 ->from('users')
                 ->where('username',$user_name_or_exam_id)
                 ->get();

           $data['count'] = $query->num_rows();
           if($data['count'] == 1){
             $data['user'] = $query->row()->username;
           }
         return $data;
      }
    }

    public function give_access_to_user_exam($user_name_or_exam_id,$password_or_supervisor_code,$type){
      if($type != "admin"){
        $query = $this->exam_db->select('AdmissionID as admission_id,ExamID as examid,StudentName as studentname,Course_Name as course,Centre_Name as centre,Course_id as course_id,InternalExamMarks as internal_marks,InternalOutOf as internal_out_of,ExamGiven as exam_given')
                 ->from('exam')
                 ->where('ExamID',$user_name_or_exam_id)
                 ->where('Centre_ID',$password_or_supervisor_code)
                 ->get();
        return $query->result_array();
      }
      else{
        $query = $this->exam_db->select('*')
                 ->from('users')
                 ->where('username',$user_name_or_exam_id)
                 ->where('password',$password_or_supervisor_code)
                 ->get();
        return $query->result_array();
      }
    }

    public function get_more_exam_status($exam_data){
        $query = $this->db->select('PROJECT_MARKS as project_marks,PROJECT_MARKS_OUT_OF as project_marks_out_of')
                 ->from('exam_master_new')
                 ->where('COURSE_ID',$exam_data[0]['course_id'])
                 ->where('ADMISSION_ID',$exam_data[0]['admission_id'])
                 ->where('EXAM_TYPE',"PRACTICAL")
                 ->limit('1')
                 ->get();
        return $query->result_array();
    }

    public function admin_credentials($username,$password){
        $query = $this->exam_db->select('ul.USER_ID,ul.EMPLOYEE_ID,ul.LOGINNAME,ul.ROLE_ID,em.CENTRE_ID,em.EMP_FNAME,em.EMP_LASTNAME,em.EMP_OFFICIAL_EMAIL as EMAIL')
                 ->from('user_login ul')
                 ->join('employee_master em','ul.EMPLOYEE_ID=em.EMPLOYEE_ID','left')
                 ->where('LOGINNAME',$username)
                 ->where('PASSWORD',$password)
                 ->limit('1')
                 ->get();
        return $query->result_array();
    }

    public function getAdminData($empId){
    	$query = $this->exam_db->select('EM.EMPLOYEE_ID,EM.EMP_FNAME,EM.EMP_LASTNAME,EM.DEPARTMENT_ID,CENTRE_ID')
    					  ->from('employee_master EM')
    					  ->where('EMPLOYEE_ID',$empId)
    					  ->get();
    	return $query->result_array();
    }
}
