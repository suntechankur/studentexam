<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

// for cron jobs do not touch
$route['todays_birthday'] = 'cron_jobs/cron_jobs_controller/today_birthdays';
$route['todays_applaud'] = 'cron_jobs/cron_jobs_controller/today_applauds';
$route['monthly_birthday_list'] = 'cron_jobs/cron_jobs_controller/monthly_employees_birthday_list';
$route['get_mails'] = 'cron_jobs/cron_jobs_controller/centre_mails';
// for cron jobs do not touch


$route['default_controller'] = 'login/login_controller/admin_credentials';
$route['404_override'] = 'errors/error_controller/page_not_found';
$route['logout'] = 'login/login_controller/logout';
$route['admin_logout'] = 'login/login_controller/admin_logout';
//$route['instructions'] = 'exam/exam_controller/setinstruction';
// for exam login
$route['instructions/(:any)'] = 'exam/exam_controller/setinstruction/$1';
$route['exam/(:any)/course/(:any)/time/(:any)/count/(:any)'] = 'exam/exam_controller/start_exam/$1/$2/$3/$4';
$route['get_question_paper_details'] = 'exam/exam_controller/get_question_paper_details';
$route['question_paper_data/(:any)'] = 'exam/exam_controller/question_paper_data/$1';
$route['exam_status/(:any)/result'] = 'exam/exam_controller/exam_status/$1';

// for admin-login
$route['admin_login/(:any)'] = 'admin/admin_controller/admin_login/$1';
$route['get_student_admission_details/(:any)'] = 'admin/admin_controller/get_student_admission_details_as_per_search/$1';
$route['get_student_subject_details/(:any)'] = 'admin/admin_controller/get_student_subject_details/$1';
$route['book_student_exam'] = 'admin/admin_controller/book_student_exam';
$route['get_required_details_and_authenticate/(:any)'] = 'admin/admin_controller/get_required_data_as_per_login/$1';
$route['get_exam_and_paper_details/(:any)'] = 'admin/admin_controller/get_exam_and_paper_details/$1';
$route['get_exam_paper_backup_by_exam_id/(:any)'] = 'admin/admin_controller/get_exam_paper_details/$1';
