$(function()
{
	$('nav#menu').mmenu(
	{
		extensions	:	[ 'effect-slide-menu', 'pageshadow' ],
		searchfield	:	true,
		counters	:	true,
		navbar 		:  	{	title : 'Main menu'	},
		navbars		:  	[
							{
								position	: 'top',
								content		: [ 'searchfield' ]
							},
							{
								position	: 'top',
								content		: [ 'prev', 'title', 'close']
							},
							{
								position	: 'bottom',
								content 	: [ '' ]
							}
						]
	});
	//$(".dash-button").hover(function()
	//{
		//$(".newclass").stop().slideDown();
		// if($(".newclass").hasClass("click-show"))
		// {
			// $(".click-show").show( "slide",{direction: "up" }, 500 );
			// $(".newclass").removeClass("click-show");
			// $(".newclass").addClass("click-hide");
		// }
		// else if($( ".newclass" ).hasClass( "click-hide" ))
		// {	
			// $(".click-hide").hide( "slide",{ direction: "up"  }, 500 );
			// $(".newclass").addClass("click-show");
			// $(".newclass").removeClass("click-hide");
		// }
	//});
	$(".dash-button,.newclass").mouseover(function()
	{
		$(".newclass").stop().slideDown();
	});
	$(".dash-button,.newclass").mouseleave(function()
	{
		$(".newclass").stop().slideUp();
	});
	
	$("#show_hide_table").click(function()
	{
		$("#st_batch").stop().slideToggle();
		var str = document.getElementById("show_hide_table").innerHTML;
		if(str == "Show Batch table")
		{
			document.getElementById("show_hide_table").innerHTML = "Hide Batch table";
		}
		if(str == "Hide Batch table")
		{
			document.getElementById("show_hide_table").innerHTML = "Show Batch table";
		}
	});
});