$("#VENDORSELECT").change(function(){
		// alert($(this).val());
		var vendId = $(this).val();
        $("#poModal").modal({backdrop: "static"});
       	$.ajax({
       		dataType: 'json',
	        url: base_url('purchase-order/purchaseOrderItem'),
	        type: 'post',
	        data: {vendId: vendId, angelos_csrf_token: csrfTkn()},
	        success: function(result){
	        	$('.vendItemTable').empty();
	        	$('.item_list').empty();
	        	$('.item_list').append('<option value="">Select Item</option>');
	        	$.each(result, function(index, item){
	        		var html = '<option value="'+item.ITEM_ID+'">'+item.ITEM_NAME+'</option>';

	        		$('.item_list').append(html);
	        	});
	        }
        });
    });

$('.servTax').click(function(){
	$('#TAXPERCENT').val('15.00');
});

$('.vat').click(function(){
	$('#TAXPERCENT').val('');
});

$('.item_list').change(function(){
		var itemId = $(this).val();
		var servTaxPer = '0.00';
		var vatPer = '0.00';
		if($('.servTax').is(':checked')){
			servTaxPer = $('#TAXPERCENT').val();
		}

		if($('.vat').is(':checked')){
			vatPer = $('#TAXPERCENT').val();
		}


        $("#poModal").modal({backdrop: "static"});
       	$.ajax({
       		dataType: 'json',
	        url: base_url('purchase-order/purchaseOrder'),
	        type: 'post',
	        data: {itemId: itemId, angelos_csrf_token: csrfTkn()},
	        success: function(result){
	        	// $('.vendItemTable').empty();
	        	$.each(result, function(index, item){
	        		var html = '<tr><td class="item_id" data-item_id="'+item.ITEM_ID+'">'+item.ITEM_NAME+'<input type="hidden" name="item_desc" class="item_desc" value="'+item.ITEM_NAME+'"></td><td><input type="text" name="used_at" class="used_at"></td><td><input type="text" name="item_desc_new" class="item_desc_new"></td><td><input type="text" name="order_qty" value="0" class="order_qty"></td><td><span class="item_unit">'+item.UNIT_NAME+'</span></td><td><input type="text" name="item_rate" value="'+item.ITEM_PRICE+'" class="item_rate"></td><td><span class="tot_cost">0.00</span></td><td><input type="text" name="serv_tax_perc" value="0" class="serv_tax_perc"></td><td><span class="vat_perc">'+vatPer+'</span></td><td><span class="ind_tax_perc">0.00</span></td></tr>';

	        		$('.vendItemTable').append(html);
	        	});
	        }
        });
});

$('.savePurchOrder').click(function(){
	$('.purchOrderData').empty();
	var item_list = $('.item_list').val();
	var item_id = $('.item_id');
	var used_at = $('.used_at');
	var item_desc = $('.item_desc');
	var order_qty = $('.order_qty');
	var item_rate = $('.item_rate');
	var serv_tax_perc = $('.serv_tax_perc');

	$("#TAXPERCENT").val($(".indTaxAmount").text());

	$('.purchOrderData').append('<input type="hidden" name="ITEMLIST" value="'+item_list+'">');

	for (var i = 0; i < item_id.length; i++) {
		var html = '<input type="hidden" name="ITEM_ID[]" value="'+$(item_id[i]).data('item_id')+'"><input type="hidden" name="USED_AT[]" value="'+$(used_at[i]).val()+'"><input type="hidden" name="ITEM_DESC[]" value="'+$(item_desc[i]).val()+'"><input type="hidden" name="ORDER_QTY[]" value="'+$(order_qty[i]).val()+'"><input type="hidden" name="ITEM_RATE[]" value="'+$(item_rate[i]).val()+'"><input type="hidden" name="SERV_TAX_PERC[]" value="'+$(serv_tax_perc[i]).val()+'">';
		$('.purchOrderData').append(html);
	}
});

$('.vendItemTable').on('keyup','.order_qty', function(){
	var item_rate = $(this).closest('tr').find('.item_rate').val();
	var tot_cost = 0;
		tot_cost = $(this).val() * item_rate;

	var indTaxAmt = ($(this).closest('tr').find('.serv_tax_perc').val()/100) * tot_cost;
	var indTaxPer = indTaxAmt.toFixed(2);

	$(this).closest('tr').find('.tot_cost').html(tot_cost);
	$(this).closest('tr').find('.ind_tax_perc').html(indTaxPer);

	var getTot = $('.tot_cost');
	var indTax = $('.ind_tax_perc');
	var totAmt = 0;
	var indTaxTot = 0;
	for (var i = 0; i <getTot.length; i++) {
		totAmt += parseFloat($(getTot[i]).text());
		indTaxTot += parseFloat($(indTax[i]).text());
	}
	var grandTot = totAmt + indTaxTot;


	$('.totalAmount').html(totAmt);
	$('.indTaxAmount').html(indTaxTot);
	$('.grandTot').html(grandTot.toFixed(2));
});

$('.vendItemTable').on('keyup','.item_rate', function(){
	var order_qty = $(this).closest('tr').find('.order_qty').val();
	var tot_cost = 0;
		tot_cost = order_qty * $(this).val();

	var indTaxAmt = ($(this).closest('tr').find('.serv_tax_perc').val()/100) * tot_cost;
	var indTaxPer = indTaxAmt.toFixed(2);

	$(this).closest('tr').find('.tot_cost').html(tot_cost);
	$(this).closest('tr').find('.ind_tax_perc').html(indTaxPer);

	var getTot = $('.tot_cost');
	var indTax = $('.ind_tax_perc');
	var totAmt = 0;
	var indTaxTot = 0;
	for (var i = 0; i <getTot.length; i++) {
		totAmt += parseFloat($(getTot[i]).text());
		indTaxTot += parseFloat($(indTax[i]).text());
	}
	var grandTot = totAmt + indTaxTot;


	$('.totalAmount').html(totAmt);
	$('.indTaxAmount').html(indTaxTot);
	$('.grandTot').html(grandTot.toFixed(2));
});
