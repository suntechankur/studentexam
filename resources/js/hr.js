$(".edit_PaidLeave").click(function () {
    //var year = $(this).closest('tr').find('.year').text();
    var empId = $(this).data('empid');
    $.ajax({
        url: base_url('hra/edit_PaidLeave'),
        dataType: 'Json',
        data: {
            //year: year,
            empId: empId,
            angelos_csrf_token: csrfTkn()},
        method: 'post',
        success: function(result)
        {

          $('.plBody').empty();
          $.each(result, function(index, item){


            if(item.YEAR == null)
              {
                item.YEAR = ''; 
              }
              if(item.APRIL == null)
              {
                item.APRIL = '0'; 
              }
              if(item.MAY == null)
              {
                item.MAY = '0'; 
              }
              if(item.JUNE == null)
              {
                item.JUNE = '0'; 
              }
              if(item.JULY == null)
              {
                item.JULY = '0'; 
              }
              if(item.AGUST == null)
              {
                item.AGUST = '0'; 
              }
              if(item.SEPTEMBER == null)
              {
                item.SEPTEMBER = '0'; 
              }
              if(item.OCTOBER == null)
              {
                item.OCTOBER = '0'; 
              }
              if(item.NOVEMBER == null)
              {
                item.NOVEMBER = '0'; 
              }
              if(item.DECEMBER == null)
              {
                item.DECEMBER = '0'; 
              }
              if(item.JANUARY == null)
              {
                item.JANUARY = '0'; 
              }
              if(item.FEBRUARY == null)
              {
                item.FEBRUARY = '0'; 
              }
              if(item.MARCH == null)
              {
                item.MARCH = '0'; 
              }
              if(item.TOTAL_PL == null)
              {
                item.TOTAL_PL = '0'; 
              }
              if(item.REMARKS == null)
              {
                item.REMARKS = ''; 
              }


            var html = '<div class="row"><div class="form-group col-lg-4"><label for="PL Year">PL Year:</label><select class="form-control year" name="YEAR"><option value="2017-2018">2017-2018</option></select></div></div><div class="row"><div class="col-lg-2"><div class="form-group"><label for="Apr">Apr:</label><input class="month april form-control" type="text" name="april" value="'+item.APRIL+'"></div></div><div class="col-lg-2"><div class="form-group"><label for="May">May:</label><input class="may form-control month" type="text" name="" value="'+item.MAY+'"></div></div><div class="col-lg-2"><div class="form-group"><label for="Jun">Jun:</label><input class="month june form-control" type="text" name="" value="'+item.JUNE+'"></div></div><div class="col-lg-2"><div class="form-group"><label for="Apr">Jul:</label><input class="month july form-control" type="text" name="" value="'+item.JULY+'"></div></div><div class="col-lg-2"><div class="form-group"><label for="May">Aug:</label><input class="month august form-control" type="text" name="" value="'+item.AGUST+'"></div></div><div class="col-lg-2"><div class="form-group"><label for="sep">Sep:</label><input class="month september form-control" type="text" name="" value="'+item.SEPTEMBER+'"></div></div><div class="col-lg-2"><div class="form-group"><label for="oct">Oct:</label><input class="month october form-control" type="text" name="" value="'+item.OCTOBER+'"></div></div><div class="col-lg-2"><div class="form-group"><label for="nov">Nov:</label><input class="month november form-control" type="text" name="" value="'+item.NOVEMBER+'"></div></div><div class="col-lg-2"><div class="form-group"><label for="dec">Dec:</label><input class="month december form-control" type="text" name="" value="'+item.DECEMBER+'"></div></div><div class="col-lg-2"><div class="form-group"><label for="jan">Jan:</label><input class="month january form-control" type="text" name="" value="'+item.JANUARY+'"></div></div><div class="col-lg-2"><div class="form-group"><label for="february">Feb:</label><input class="month february form-control" type="text" name="" value="'+item.FEBRUARY+'"></div></div><div class="col-lg-2"><div class="form-group"><label for="mar">Mar:</label><input class="month march form-control" type="text" name="" value="'+item.MARCH+'"></div></div></div><div class="row"><div class="col-lg-6"><div class="form-group"><label for="Total PL">Total PL:</label><input class="total_pl form-control" type="text" name="" value="'+item.TOTAL_PL+'"></div></div><div class="col-lg-6"><div class="form-group"><label for="Remarks">Remarks:</label><input class="remarks form-control" type="text" name="" value="'+item.REMARKS+'"></div></div></div>';

            $('.plBody').append(html);


//// Start Current Month And Year ///////
var financialYearBegin = "2017"; //January is 0!
var financialYearEnd = "2018";
//// End Current Month And Year ///////

var firstFinancialMonth = 4;
var firstFinancialDate = new Date(financialYearBegin, firstFinancialMonth-1, 1);
var firstFinancialDate = moment(firstFinancialDate);
var compareFinancialDate = firstFinancialDate.add(-6, 'months');
var DATEOFJOIN = moment(item.DATEOFJOIN);
var joining = new Date(DATEOFJOIN);

if(joining >= compareFinancialDate)
{
var joiningDate = joining.getDate();
var joiningMonth = joining.getMonth()+1;
var joiningYear = joining.getFullYear();


var joinDMY = joiningDate+"/"+joiningMonth+"/"+joiningYear;
var monthsToAdd = 0;

var previousMonthsToAdd = 0;
// if(joiningYear +1 == financialYearBegin)
// {
//   previousMonthsToAdd = -firstFinancialMonth;  
//   alert("Previous Months to Add "+previousMonthsToAdd);
// }
// else
// {
//   previousMonthsToAdd = joiningMonth - firstFinancialMonth;
//   alert("Previous Months to Add "+previousMonthsToAdd);
// }

var afterSixMonJoin = new Date();
var monthAdditionCoefficient = 0;
//var monthsAfterAdjustment = null;
if(joiningDate!="01")
{
  monthAdditionCoefficient = 7;
}
else
{
  monthAdditionCoefficient = 6;
}

if(joiningYear +1 == financialYearBegin)
{
  previousMonthsToAdd = -12 - firstFinancialMonth + joiningMonth;  
  // alert("Previous Months to Add "+previousMonthsToAdd);
}
else
{
  previousMonthsToAdd = joiningMonth - firstFinancialMonth;
  // alert("Previous Months to Add "+previousMonthsToAdd);
}


  monthsToAdd = monthAdditionCoefficient + previousMonthsToAdd; 
  alert(monthsToAdd);
  //alert("You are  at level 1 Date of Join");
  //alert(DATEOFJOIN);   
  afterSixMonJoin = DATEOFJOIN.add( - previousMonthsToAdd, 'months');
  // alert("You are  at level 2 After six months of Date of Joining");
  // alert(afterSixMonJoin);  
  afterSixMonJoin = afterSixMonJoin.add(monthsToAdd, 'months');
  // alert("You are  at level 3 After six months of Date of Joining with Months to add");
  // alert(afterSixMonJoin);


var afterSixMonDate = new Date(afterSixMonJoin);
var afterSixDate = afterSixMonDate.getDate();
var afterSixMonMonth = afterSixMonDate.getMonth();
var aftersixmonthYear = afterSixMonDate.getFullYear();
var afteradd = afterSixDate+"/"+afterSixMonMonth+"/"+aftersixmonthYear;
alert("FMonth to Add "+monthsToAdd);
            var FY = '';
            FY = financialYearBegin+'-'+financialYearEnd;
              
            $('.year').val(FY);

            for(var i = monthsToAdd; i >=1; i--)
              {
                
                var afterSixMonDate = new Date(afterSixMonJoin);
                var afterSixMonthMom = moment(afterSixMonDate);
                var dateMonth = afterSixMonMonth+'-'+aftersixmonthYear;
                 dateMonth = afterSixMonthMom.add(-1, 'months'); 

                var dateList = new Date(dateMonth);
                var additionalMonths = dateList.getMonth()+1;
                var monthNum = additionalMonths;   
                var monthName = moment.months(monthNum - 1); 
                 monthName = monthName.toLowerCase();
                 alert("Month loop is" + monthName);
                 $('.'+monthName).attr('readonly',true);
                  afterSixMonJoin = dateMonth;
                  
              }
            }

          });
          $('.updatePl').removeAttr('data-empid');
          $('.updatePl').attr('data-empid',empId);
        }
      });
    });

$('.plBody').on('keyup','.month', function(){

   // initialize the sum (total Month) to zero
    var sum = 0;

    // we use jQuery each() to loop through all the textbox with 'Month' class
    // and compute the sum for each loop
    $('.month').each(function() {
        sum += Number($(this).val());
        
    });

    // set the computed value to 'totalPL' textbox
    $('.total_pl').val(sum);

});

$('.updatePl').click(function(){
  var empId = $(this).data('empid');
  $.ajax({
        url: base_url('hra/updatePL'),
        // dataType: 'Json',
        data: {
            empId: empId,
            YEAR: $('.year').val(),
            APRIL: $('.april').val(),
            MAY: $('.may').val(),
            JUNE: $('.june').val(),
            JULY: $('.july').val(),
            AGUST: $('.august').val(),
            SEPTEMBER: $('.september').val(),
            OCTOBER: $('.october').val(),
            NOVEMBER: $('.november').val(),
            DECEMBER: $('.december').val(),
            JANUARY: $('.january').val(),
            FEBRUARY: $('.february').val(),
            MARCH: $('.march').val(),
            TOTAL_PL: $('.total_pl').val(),
            REMARKS: $('.remarks').val(),
            angelos_csrf_token: csrfTkn()
          },
        method: 'post',
        success: function(result)
        {
          // var url = window.location.href;
          
          // window.location = url;
        }
      });
});

function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#profPic').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$('#profImg').change(function(){
  readURL(this);
});