
$('.add_follow_up').on('click',function(){
    var telEnqId = $(this).data('fid');
    var CENTRE_ID = $('#CENTRE_ID').val();
    var CENTRE_NAME = $('#CENTRE_ID option:selected').text();

    var FOLLOWUP_DATE = $('#FOLLOWUP_DATE').val();

    var FOLLOWUP_BY = $('#FOLLOWUP_BY').val();
    var FOLLOWUP_BY_NAME = $('#FOLLOWUP_BY option:selected').text();

    var FOLLOWUP_DETAILS =$('#FOLLOWUP_DETAILS').val();

    var FROM_TIME = $("#from_time").val();
    var TO_TIME = $("#to_time").val();

    var PREFERRED_TIME = "";
    if(FROM_TIME != "" && TO_TIME != ""){
        PREFERRED_TIME = FROM_TIME + ' - ' + TO_TIME;
    }

    var STATUS = $('#STATUS').val();
    var STATUS_SELECTED = $('#STATUS option:selected').text();

    var NEXT_FOLLOWUP_DATE = '';
    var VISIT_DATE = $('#VISIT_DATE').val();

    var today = new Date();
    today.setDate(today.getDate()+1)
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    var currentDate = dd+'/'+mm+'/'+yyyy;

    var follow_up_date = $('#NEXT_FOLLOWUP_DATE').val();
    if(follow_up_date != ""){
        NEXT_FOLLOWUP_DATE = $('#NEXT_FOLLOWUP_DATE').val();
    }
    // else if(STATUS == 3){
    //     NEXT_FOLLOWUP_DATE = $('#NEXT_FOLLOWUP_DATE').val(today);
    // }
    else{
        NEXT_FOLLOWUP_DATE = currentDate;
    }
  //  alert(NEXT_FOLLOWUP_DATE);

    $.ajax({
            url: $(this).data('url'),
            dataType: 'JSON',
            data: {
                angelos_csrf_token: csrfTkn(),
                telEnqId:telEnqId,
                CENTRE_ID: CENTRE_ID,
                FOLLOWUP_DATE: FOLLOWUP_DATE,
                FOLLOWUP_BY: FOLLOWUP_BY,
                FROM_TIME: FROM_TIME,
                TO_TIME: TO_TIME,
                FOLLOWUP_DETAILS: FOLLOWUP_DETAILS,
                STATUS: STATUS,
                NEXT_FOLLOWUP_DATE: NEXT_FOLLOWUP_DATE,
                VISIT_DATE: VISIT_DATE
            },
            type: 'post',
            beforeSend: function(){
              $("#wait").show();
            },
            complete: function(){
              $("#wait").hide();
            },
            success: function(result)
            {
                $("#STATUS, #FOLLOWUP_DETAILS, #FOLLOWUP_BY").removeAttr('style');
                // alert(result);
                if (!result.errors) {
                    var html = '<tr data-followUpId="'+result+'"><td><span class="del_data" data-url="'+base_url('tele-enquiry-follow-up/deleteTeleEnquiryFollowUp')+'"><span class="glyphicon glyphicon-trash"></span></span></td><td><a href="#" class="centreName" data-source="centreDropDown" data-prepend="Select Centre">'+CENTRE_NAME+'</a></td><td><a href="#" class="followUpDate">'+FOLLOWUP_DATE+'</a></td><td><a class="followUpBy" href="#" data-source="employeeNames" data-prepend="Select Employees">'+FOLLOWUP_BY_NAME+'</a></td><td><span><a class="set_from_time" href="#" value="8:30 am">'+FROM_TIME+'</a></span> - <span><a class="set_to_time" href="#">'+TO_TIME+'</a></span></td><td><a href="#" class="FOLLOWUP_DETAILS">'+FOLLOWUP_DETAILS+'</a></td><td><a href="#" class="STATUS">'+STATUS_SELECTED+'</a></td><td><a href="#" class="nextFollowUpDate">'+NEXT_FOLLOWUP_DATE+'</a></td><td><a href="" class="visitDate">'+VISIT_DATE+'</a></td></tr>';
                    // $(html).insertAfter('.followUpTable > tbody > tr:last');
                    $(html).insertBefore('#followupDetailsOperation');
                    location.reload();
                    editableColumns();
                }

                if (result.errors) {
                    if(result.errors.length > 0){
                            $.each(result.errors, function(index, item){
                                $.each(item, function(key, value){
                                    $( "#"+key ).css('border','2px solid red');
                                });
                            });
                        }
                }
            }
           });
});

$('.setDate').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    locale: {
        format: 'DD/MM/YYYY'
    }
});

$('#STATUS').change(function(){
    var today = new Date();
    // if($(this).val() == 3){
    //    today.setDate(today.getDate()+1)
    // }
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    var currentDate = dd+'/'+mm+'/'+yyyy;
    if($(this).val() == 6){
        $('#NEXT_FOLLOWUP_DATE').val('');
    }
    else{
        $('#NEXT_FOLLOWUP_DATE').val('');
    }

    if(this.value==6)
    {
        $("#VISIT_DATE").removeAttr("disabled");
    }
    else{
        $("#VISIT_DATE").attr("disabled", "disabled");
    }
});

editableColumns();

function editableColumns(){

    $('.centreName').editable({
        type: 'select',
        pk: function(){
          return $(this).closest('tr').attr('data-followUpId')
        },
        params:{angelos_csrf_token: csrfTkn()},
        url: '/tele-enquiry-follow-up/updateTeleEnquiryFollowUp',
        title: 'Select Centre name',
        name: 'CENTRE_ID'
    });

    $('.followUpDate').editable({
        type: 'combodate',
        format: 'YYYY-MM-DD',
        viewformat: 'DD-MM-YYYY',
        template: 'DD-MM-YYYY',
        combodate: {
            maxYear: 2021,
            minuteStep: 1
        },
        pk: function(){
          return $(this).closest('tr').attr('data-followUpId')
        },
        params:{angelos_csrf_token: csrfTkn()},
        url: '/tele-enquiry-follow-up/updateTeleEnquiryFollowUp',
        name: 'FOLLOWUP_DATE',
        title: 'Follow up date'
    });

    $('.followUpBy').editable({
        type: 'select',
        pk: function(){
          return $(this).closest('tr').attr('data-followUpId')
        },
        params:{angelos_csrf_token: csrfTkn()},
        url: '/tele-enquiry-follow-up/updateTeleEnquiryFollowUp',
        name: 'FOLLOWUP_BY'
    });

    $('.set_from_time').editable({
        type: 'combodate',
        format: 'hh: mm: a',
        viewformat: 'hh: mm: a',
        template: 'hh: mm: a',
        combodate: {
            minuteStep: 1
        },
        params:{angelos_csrf_token: csrfTkn()},
        pk: function(){
          return $(this).closest('tr').attr('data-followUpId')
        },
        url: '/tele-enquiry-follow-up/updateTeleEnquiryFollowUp',
        name: 'set_from_time'
    });

    $('.set_to_time').editable({
        type: 'combodate',
        format: 'hh: mm: a',
        viewformat: 'hh: mm: a',
        template: 'hh: mm: a',
        combodate: {
            minuteStep: 1
        },
        pk: function(){
          return $(this).closest('tr').attr('data-followUpId')
        },
        params:{angelos_csrf_token: csrfTkn()},
        url: '/tele-enquiry-follow-up/updateTeleEnquiryFollowUp',
        name: 'set_to_time'
    });

    $('.FOLLOWUP_DETAILS').editable({
        type: 'text',
        pk: function(){
          return $(this).closest('tr').attr('data-followUpId')
        },
        params:{angelos_csrf_token: csrfTkn()},
        url: '/tele-enquiry-follow-up/updateTeleEnquiryFollowUp',
        name: 'FOLLOWUP_DETAILS',
        title: 'Follow up by'
    });

    $('.STATUS').editable({
        type: 'select',
        source: base_url('followUpStatus'),
        pk: function(){
          return $(this).closest('tr').attr('data-followUpId')
        },
        params:{angelos_csrf_token: csrfTkn()},
        autotext: 3,
        url: base_url('tele-enquiry-follow-up/updateTeleEnquiryFollowUp'),
        name: 'STATUS',
        title: 'Select Status'
    });

    $('.nextFollowUpDate').editable({
        type: 'combodate',
        format: 'YYYY-MM-DD',
        viewformat: 'DD-MM-YYYY',
        template: 'DD-MM-YYYY',
        combodate: {
            maxYear: 2021,
            minuteStep: 1
        },
        pk: function(){
          return $(this).closest('tr').attr('data-followUpId')
        },
        params:{angelos_csrf_token: csrfTkn()},
        url: base_url('tele-enquiry-follow-up/updateTeleEnquiryFollowUp'),
        name: 'NEXT_FOLLOWUP_DATE',
        title: 'Next follow up date'
    });

    $('.visitDate').editable({
        type: 'combodate',
        format: 'YYYY-MM-DD',
        viewformat: 'DD-MM-YYYY',
        template: 'DD-MM-YYYY',
        combodate: {
            maxYear: 2021,
            minuteStep: 1
        },
        pk: function(){
          return $(this).closest('tr').attr('data-followUpId')
        },
        params:{angelos_csrf_token: csrfTkn()},
        url: '/tele-enquiry-follow-up/updateTeleEnquiryFollowUp',
        name: 'VISIT_DATE',
        title: 'Visit Date'
    });

}

$('.followUpTable').on('click','.del_data',function(){
   var id  = $(this).closest('tr').attr('data-followUpId');
   var telEnqId = $(this).closest('tr').attr('data-telEnqId');
   var here = $(this);
   $.ajax({
        url: $(this).data('url'),
        data: {data_id: id, tel_enq_id: telEnqId, angelos_csrf_token: csrfTkn()},
        type: 'post',
        success: function(result){
            here.closest('tr').remove();
        }
    });
});
